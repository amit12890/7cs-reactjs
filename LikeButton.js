import React, { Component } from "react"
import {
  Alert,
  Image,
  Text,
  AsyncStorage,
  TouchableOpacity
} from "react-native"
import HudView from "react-native-easy-hud"
const USER_ID = "user_id"

class LikeButton extends Component {
  constructor(props) {
    super(props)
    this.state = {
      id: props.id,
      liked: props.liked || false
    }
  }

  _hud: HudView

  render() {
    if (this.state.liked) {
      return (
        <TouchableOpacity onPress={this._handlePress.bind(this)}>
          <Image
            source={require("./img/heart1.png")}
            style={{ width: 30, height: 30 }}
          />
        </TouchableOpacity>
      )
    } else {
      return (
        <TouchableOpacity onPress={this._handlePress.bind(this)}>
          <Image
            source={require("./img/heart2.png")}
            style={{ width: 30, height: 30 }}
          />
        </TouchableOpacity>
      )
    }
  }

  async _handlePress() {
    try {
      let uid = await AsyncStorage.getItem(USER_ID)

      let like_status = ""
      // this.redireu
      if (this.state.liked) {
        like_status = "false"
      } else {
        like_status = "true"
      }

      let response = await fetch("http://b2bapp.7csgold.com/api/wishlist.php", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          product_id: this.state.id,
          user_id: uid,
          status: like_status
        })
      })
      let res = await response.json()
      if (response.status >= 200 && response.status < 300) {
        if (res.status) {
        } else {
          Alert.alert("Something went wrong", "Try again after sometime1")
        }

        // this.redirect('root');
      } else {
        Alert.alert("Something went wrong", "Try again after sometime2")
      }
    } catch (error) {
      //this.redirect('login');
      console.log(error)
      Alert.alert("Something went wrong", "Try again after sometime3")
    }
    this.setState({
      liked: this.state.liked ? false : true
    })
  }
}

module.exports = LikeButton
