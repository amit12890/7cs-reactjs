import { Permissions, Notifications } from 'expo';
import { AsyncStorage } from 'react-native';
import axios from 'axios';

const USER_ID = 'user_id';

const PUSH_ENDPOINT = 'http://b2bapp.7csgold.com/api/store_token.php';

export default async () => {

  let previousToken = await AsyncStorage.getItem('pushtoken');

  if (previousToken) {
    console.log("exising");
    console.log(previousToken);
    return;
  } else {

    let {status} = await Permissions.askAsync(Permissions.REMOTE_NOTIFICATIONS);

    if (status !== 'granted') {
      console.log("newbie");
      return;
    }


    let token = await Notifications.getExpoPushTokenAsync();
    let user_id = await AsyncStorage.getItem(USER_ID);
    try {
      console.log("register token");
      let response = await fetch(PUSH_ENDPOINT, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({

          token: token,
          user_id: user_id


        })
      });
      let res = await response.json();
      if (response.status >= 200 && response.status < 300) {
        console.log(res);

      } else {

        console.log("error token" + res.error);
      }
      // this.redirect('root');


    } catch (error) {
      console.log("error: " + error)
    }


    console.log("token" + token);
    AsyncStorage.setItem('pushtoken', token);
  }

};
