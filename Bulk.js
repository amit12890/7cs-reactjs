import React, { Component } from "react"
import {
  View,
  Text,
  StyleSheet,
  Button,
  Image,
  ListView,
  ScrollView,
  AsyncStorage,
  ActivityIndicator,
  TouchableHighlight,
  Alert
} from "react-native"
import HudView from "react-native-easy-hud"
//import Upload from './Upload';

const USER_ID = "user_id"

export default class BulkScreen extends Component {
  constructor(props) {
    super(props)

    var ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
    this.state = {
      clonelist: ds.cloneWithRows([])
    }
  }

  async componentWillMount() {
    // console.log("hello");
    const { params } = this.props.navigation.state
    const ref_id = params.ref_id
    const user_id = await AsyncStorage.getItem(USER_ID)
    let url = "http://b2bapp.7csgold.com/api/get_bulk.php?id=" + ref_id
    console.log(url)

    await fetch(url, {
      method: "GET"
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log(responseJson)
        var ds = new ListView.DataSource({
          rowHasChanged: (r1, r2) => r1 !== r2
        })
        this.setState({
          clonelist: ds.cloneWithRows(responseJson.images)
        })
      })
      .catch(error => {
        console.error(error)
      })
  }

  _hud: HudView

  render() {
    return (
      <View
        style={{
          flex: 1,
          margin: 20
        }}
      >
        <ListView
          dataSource={this.state.clonelist}
          renderRow={rowData => (
            <View
              style={{
                marginHorizontal: 10,
                padding: 10,
                marginBottom: 15,
                paddingTop: 20,
                backgroundColor: "#fff"
              }}
            >
              <Image
                style={styles.stretch}
                source={{
                  uri: rowData.img_url
                }}
              />
            </View>
          )}
          enableEmptySections={true}
        />

        <HudView
          ref={hud => {
            this._hud = hud
          }}
          delay={1.5}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'flex-start',
    alignItems: "center",
    backgroundColor: "rgb(248,248,248)"
  },
  activityContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 70
  },
  activityIndicator: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    height: 80
  },
  input: {
    height: 50,
    width: 300,
    marginTop: 10,
    paddingHorizontal: 10,
    paddingVertical: 10,
    backgroundColor: "#eee",
    borderBottomColor: "#eee",
    borderRadius: 10,
    bottom: 20,
    fontSize: 18
  },
  stretch: {
    width: 300,
    height: 300,
    alignSelf: "center"
  },
  button: {
    height: 50,
    backgroundColor: "rgb(30,75,113)",
    borderRadius: 50,

    alignSelf: "stretch",
    marginTop: 10,
    marginBottom: 10,
    justifyContent: "center"
  },
  Sbutton: {
    height: 50,
    backgroundColor: "rgb(30,75,113)",
    borderRadius: 50,

    alignSelf: "stretch",

    marginBottom: 10,
    justifyContent: "center"
  },

  buttonText: {
    fontSize: 18,
    color: "#FFF",
    alignSelf: "center"
  },
  heading: {
    fontSize: 30
  },
  error: {
    color: "red",
    paddingTop: 10
  },
  loader: {
    marginTop: 20
  }
})
