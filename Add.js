import React, { Component } from "react"
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Button,
  Alert,
  Image,
  Keyboard,
  ActivityIndicator,
  ListView,
  AsyncStorage,
  TouchableHighlight,
  Picker
} from "react-native"
import { ImagePicker } from "expo"
import Ionicons from "react-native-vector-icons/FontAwesome"
import md5 from "react-native-md5"
import HudView from "react-native-easy-hud"

const ACCESS_TOKEN = "access_token"
const USER_ID = "user_id"
const TYPE = "type"
export default class Add extends Component {
  static navigationOptions = {
    tabBarLabel: "Add",
    tabBarIcon: () => (
      <Ionicons
        style={{ alignSelf: "center" }}
        name="plus-circle"
        size={24}
        color="rgb(30,75,113)"
      />
    )
  }

  constructor(props) {
    super(props)

    this.state = {
      loading: false,
      uid: "",
      email: "",
      accessToken: "",
      type: "",
      result: "",
      weight: "",
      karatage: "22",
      remarks: "",
      success: "",
      bottom: 0,
      errors: "",
      image: null
    }
  }

  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      this._keyboardDidShow.bind(this)
    )
    this.keyboardDidHideListener = Keyboard.addListener(
      "keyboardDidHide",
      this._keyboardDidHide.bind(this)
    )
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove()
    this.keyboardDidHideListener.remove()
  }

  _keyboardDidShow() {
    // console.log(this.state.bottom);
    this.setState({ bottom: 130 })
  }

  _keyboardDidHide() {
    this.setState({ bottom: 0 })
  }

  async setSession() {
    try {
      const user_id = await AsyncStorage.getItem(USER_ID)
      if (user_id !== null) {
        // We have data!!
        console.log("Set Session")
        console.log(user_id)

        this.setState({ uid: user_id })
      }
    } catch (error) {
      console.log(error)
      // Error retrieving data
    }
  }

  async componentDidMount() {
    // console.log("hello");
    await this.setSession()
  }

  async placeOrder() {
    if (this.state.image) {
      let rand = md5.b64_md5(Date.now() + this.state.uid)
      let img = rand + ".jpg"

      this.setState({ loading: true })
      var data = new FormData()
      data.append("image", {
        uri: this.state.image,
        name: img,
        type: "image/jpeg"
      })
      data.append("weight", this.state.weight)
      data.append("remarks", this.state.remarks)
      data.append("karatage", this.state.karatage)
      data.append("user_id", this.state.uid)

      const config = {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data"
        },
        body: data
      }

      try {
        let response = await fetch(
          "http://b2bapp.7csgold.com/api/vendor_post_order.php",
          config
        )
        let res = await response.json()

        if (response.status >= 200 && response.status < 300) {
          //Handle success
          if (res.status) {
            let status = res.status

            console.log(status)

            if (status) {
              this.setState({ loading: false })
              this.setState({ image: null })
              this.setState({ weight: "" })
              this.setState({ remarks: "" })

              const { navigate } = this.props.navigation
              this.setState({ success: "Product Posted" })
              let type = await AsyncStorage.getItem(TYPE)
              if (type == "vendor") {
                navigate("VendorPending")
              } else {
                navigate("Employee")
              }
            } else {
              this.setState({ loading: false })
              Alert.alert("Problem Occured", res.error)
              this.setState({ errors: res.error })
            }
          } else {
            this.setState({ loading: false })
            Alert.alert("Problem Occured", res.error)
            this.setState({ errors: res.error })
          }

          //this.redirect('home');
        } else {
          //Handle error
          this.setState({ loading: false })
          let error = res
          throw error
          Alert.alert("Problem Occured", res.error)
          this.setState({ errors: res.error })
        }
      } catch (error) {
        this.setState({ loading: false })
        this.setState({ error: error })
        console.log("error " + error)
        this.setState({ errors: error })
        Alert.alert("Problem Occured", error)
        this.setState({ showProgress: false })
      }
    } else {
      Alert.alert("Problem Occured", "Kindly Fill in All Details")
      this.setState({ errors: "Kindly Fill in All Details" })
    }
  }
  _takePhoto = async () => {
    let result = await ImagePicker.launchCameraAsync({
      allowsEditing: true,

      quality: 0.5
    })

    console.log(result)

    if (!result.cancelled) {
      this.setState({ image: result.uri })
    }
  }
  _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,

      quality: 0.5
    })

    console.log(result)

    if (!result.cancelled) {
      this.setState({ image: result.uri })
    }
  }

  _hud: HudView

  render() {
    let { image } = this.state
    const loading = this.state.loading
    return (
      <View style={{ flex: 1, paddingTop: 25, bottom: this.state.bottom }}>
        <View
          style={{
            padding: 10,
            backgroundColor: "#fff",
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <Text style={{ fontSize: 20, color: "rgb(30,75,113)" }}>
            7Cs Gold - Add Products
          </Text>
        </View>

        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            marginBottom: 1,
            marginHorizontal: 10,
            marginTop: 10,
            backgroundColor: "#fff",
            padding: 10
          }}
        >
          <TouchableHighlight underlayColor={"#fff"} onPress={this._pickImage}>
            <Ionicons
              style={{ alignSelf: "center", marginRight: 15 }}
              name="image"
              size={24}
              color="rgb(30,75,113)"
            />
          </TouchableHighlight>
          <TouchableHighlight underlayColor={"#fff"} onPress={this._takePhoto}>
            <Ionicons
              style={{ alignSelf: "center", marginLeft: 15 }}
              name="camera"
              size={24}
              color="rgb(30,75,113)"
            />
          </TouchableHighlight>
        </View>
        <View
          style={{
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: "#fff",
            marginHorizontal: 10
          }}
        >
          {image && (
            <Image
              source={{ uri: image }}
              style={{ width: 100, height: 100, margin: 10 }}
            />
          )}
        </View>

        <View
          style={{
            paddingVertical: 20,
            marginHorizontal: 10,
            paddingHorizontal: 20,
            borderWidth: 1,
            backgroundColor: "#fff",
            borderColor: "#fff",
            borderTopWidth: 1,
            borderTopColor: "#c0c0c0"
          }}
        >
          <View style={{ flexDirection: "row", alignSelf: "stretch" }}>
            <Text style={{ fontSize: 16, color: "rgb(30,75,113)", flex: 2 }}>
              Karatage
            </Text>
            <Text style={{ fontSize: 16, color: "rgb(30,75,113)", flex: 1 }}>
              :
            </Text>

            <Picker
              selectedValue={this.state.karatage}
              onValueChange={karatage => this.setState({ karatage: karatage })}
              mode="dropdown"
              style={{
                width: 150,

                borderWidth: 1,
                borderColor: "#fff",
                borderBottomWidth: 1,
                borderBottomColor: "rgb(30,75,113)",
                bottom: 12
              }}
            >
              <Picker.Item label="22" value="22" />
              <Picker.Item label="21" value="21" />
              <Picker.Item label="18" value="18" />
            </Picker>
          </View>

          <View style={{ flexDirection: "row", alignSelf: "stretch" }}>
            <Text style={{ fontSize: 16, color: "rgb(30,75,113)", flex: 2 }}>
              Size/Weight
            </Text>
            <Text style={{ fontSize: 16, color: "rgb(30,75,113)", flex: 1 }}>
              :
            </Text>

            <TextInput
              style={styles.input}
              onChangeText={text => this.setState({ weight: text })}
              value={this.state.weight}
              placeholder="Size/Weight/Qty"
            />
          </View>

          <View style={{ flexDirection: "row", alignSelf: "stretch" }}>
            <Text
              style={{
                fontSize: 16,
                color: "rgb(30,75,113)",
                flex: 2,
                fontWeight: "bold"
              }}
            >
              Remarks
            </Text>
            <Text
              style={{
                fontSize: 16,
                color: "rgb(30,75,113)",
                flex: 1,
                fontWeight: "bold"
              }}
            >
              :
            </Text>

            <TextInput
              style={styles.inputBig}
              multiline={true}
              numberOfLines={4}
              onChangeText={text => this.setState({ remarks: text })}
              value={this.state.remarks}
              placeholder="Remarks"
            />
          </View>

          <View style={{ marginTop: 10, flexDirection: "row", height: 60 }}>
            {loading ? (
              <ActivityIndicator
                animating={loading}
                color="rgb(30,75,113)"
                size="large"
                style={styles.activityIndicator}
              />
            ) : (
              <TouchableHighlight
                underlayColor={"#fff"}
                style={{
                  flex: 1,
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "center"
                }}
                onPress={() => {
                  this.placeOrder()
                }}
              >
                <Text style={styles.modalText}>Post </Text>
              </TouchableHighlight>
            )}
          </View>
          <Text style={{ textAlign: "center" }}>{this.state.success}</Text>
        </View>

        <HudView
          ref={hud => {
            this._hud = hud
          }}
          delay={1.5}
        />
      </View>
    )
  }
}

const Errors = props => {
  return (
    <View>
      <Text style={styles.error}> {props.errors} </Text>
    </View>
  )
}

const styles = StyleSheet.create({
  error: {
    color: "red",
    paddingTop: 10,
    textAlign: "center"
  },
  input: {
    width: 150,
    padding: 10,
    bottom: 12
  },

  inputBig: {
    width: 150,
    padding: 10,
    bottom: 17,
    height: 70
  },

  activityIndicator: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  modalText: {
    color: "#fff",
    borderRadius: 40,
    width: 150,
    paddingVertical: 10,
    fontSize: 22,
    textAlign: "center",
    borderWidth: 1,
    borderColor: "rgb(30,75,113)",
    backgroundColor: "rgb(30,75,113)"
  },
  text: {
    color: "#fff",
    borderRadius: 40,
    width: 150,
    padding: 5,
    fontSize: 18,
    textAlign: "center",
    borderWidth: 1,
    borderColor: "rgb(30,75,113)",
    backgroundColor: "rgb(30,75,113)"
  }
})
