import React, { Component } from "react"
import { TextInput, View, Text, StyleSheet, Button, Image, ListView, AsyncStorage, ActivityIndicator, TouchableHighlight, RefreshControl, Modal, Share, Alert, Keyboard } from "react-native"
import Ionicons from "react-native-vector-icons/FontAwesome"
import Imagezoom from "react-native-transformable-image"
import FitImage from "react-native-fit-image"
import Dimensions from "Dimensions"
import HudView from "react-native-easy-hud"
import { Audio, WebBrowser } from "expo"
import DateTimePicker from "react-native-modal-datetime-picker"
import PopupDialog, { DialogTitle, DialogButton } from "react-native-popup-dialog"
import TimerMixin from 'react-timer-mixin';

const {width, height} = Dimensions.get("window")
const soundObject = new Expo.Audio.Sound()
const ACCESS_TOKEN = "access_token"
const USER_ID = "user_id"
export default class Order extends Component {
  static navigationOptions = {
    tabBarLabel: "Order",
    tabBarIcon: () => (
      <Ionicons
      style={{
        alignSelf: "center"
      }}
      name="shopping-bag"
      size={24}
      color="rgb(30,75,113)"
      />
    )
  }

  constructor(props) {
    console.log("called")
    super(props)
    var ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
    this.state = {
      refreshing: false,
      email: "",
      accessToken: "",
      type: "",
      result: "",
      modalVisible: false,
      modaluri: "",
      filter: "order",
      orderBg: "rgb(30,75,113)",
      bulkorderBg: "#fff",
      orderText: "#fff",
      bulkorderText: "rgb(30,75,113)",
      clonelist: ds.cloneWithRows([]),
      switch: false,
      images: [],
      isDateTimePickerVisible: false,
      isSearchFilter: false,
      filterStartDate: "",
      filterEndDate: "",
      minimumDate: new Date("1970-01-01"),
      isAudioPlaying: false,
      audioId: -999
    }

    Audio.setIsEnabledAsync(true)
    Audio.setAudioModeAsync({
      allowsRecordingIOS: false,
      interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
      playsInSilentLockedModeIOS: true,
      shouldDuckAndroid: true,
      interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
      playsInSilentModeIOS: true
    })
  }

  _askForPermissions() {
    const {Permissions} = Expo
    const {response} = Permissions.askAsync(Permissions.AUDIO_RECORDING)
  // this.setState({
  //   haveRecordingPermissions: response.status === "granted"
  // })
  }

  _onRefresh() {
    console.log(this.state.filter)
    this.setState({
      refreshing: true
    })
    let user_id = this.state.uid
    console.log("USer id " + user_id)
    let url = "http://b2bapp.7csgold.com/api/customer_order_history.php?id=" +
    user_id +
    "&filter=" +
    this.state.filter
    fetch(url, {
      method: "GET"
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log(responseJson)
        var ds = new ListView.DataSource({
          rowHasChanged: (r1, r2) => r1 !== r2
        })
        this.setState({
          clonelist: ds.cloneWithRows(responseJson.product),
          images: responseJson.product.images
        })
        console.log(this.state.images)
      })

      .catch(error => {
        console.error(error)
      })
    this.setState({
      refreshing: false
    })
  }

  setModalVisible(visible, img) {
    console.log(img)
    this.setState({
      modalVisible: visible
    })
    this.setState({
      modaluri: img
    })
  }

  async setSession() {
    try {
      const value = await AsyncStorage.getItem(ACCESS_TOKEN)
      const user_id = await AsyncStorage.getItem(USER_ID)
      if (value !== null) {
        // We have data!!
        console.log("Set Session")
        console.log(user_id)
        this.setState({
          accessToken: value
        })
        this.setState({
          uid: user_id
        })
      }
    } catch (error) {
      console.log(error)
    // Error retrieving data
    }
  }
  async storeToken(responseData) {
    console.log(responseData)
    try {
      await AsyncStorage.setItem(ACCESS_TOKEN, responseData)
    } catch (error) {
      console.log("storage error")
    // Error saving data
    }
  }

  async getToken() {
    try {
      const value = await AsyncStorage.getItem(ACCESS_TOKEN)
      if (value !== null) {
        // We have data!!
        console.log("has storage")
        console.log(value)
      }
    } catch (error) {
      console.log(error)
    // Error retrieving data
    }
  }

  componentWillUnmount() {
    this.pauseAudio("")
  }

  async componentDidMount() {
    console.log("hello")
    await this.setSession()
    let user_id = this.state.uid
    console.log("USer id " + user_id)
    let url = "http://b2bapp.7csgold.com/api/customer_order_history.php?id=" +
    user_id +
    "&filter=" +
    this.state.filter
    fetch(url, {
      method: "GET"
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log(responseJson)
        var ds = new ListView.DataSource({
          rowHasChanged: (r1, r2) => r1 !== r2
        })
        this.setState({
          clonelist: ds.cloneWithRows(responseJson.product)
        })
      })

      .catch(error => {
        console.error(error)
      })

    this._askForPermissions()
  }
  async getOrder(filter, searchId = "") {
    this.pauseAudio("")
    Keyboard.dismiss()
    this._hud.show()
    let user_id = this.state.uid
    console.log(filter)
    let url = "http://b2bapp.7csgold.com/api/customer_order_history.php?id=" +
    user_id +
    "&filter=" +
    this.state.filter
    if (this.state.filterStartDate.length > 0) {
      url += "&start_date=" + encodeURIComponent(this.state.filterStartDate) + ""
    }
    if (this.state.filterEndDate.length > 0) {
      url += "&end_date=" + encodeURIComponent(this.state.filterEndDate) + ""
    }
    if (searchId.length > 0) {
      url += "&search_id=" + encodeURIComponent(searchId) + ""
    }

    console.log(url)
    fetch(url, {
      method: "GET"
    })
      .then(response => response.json())
      .then(responseJson => {
        this._hud.hide()

        console.log(responseJson)
        var ds = new ListView.DataSource({
          rowHasChanged: (r1, r2) => r1 !== r2
        })
        this.setState({
          clonelist: ds.cloneWithRows(responseJson.product)
        })
      })

      .catch(error => {
        this._hud.hide()
        console.error(error)
      })
  }
  async setOrder() {
    this.setState({
      switch: true
    })
    await this.setState({
      filter: "order",
      orderBg: "rgb(30,75,113)",
      bulkorderBg: "#fff",
      orderText: "#fff",
      bulkorderText: "rgb(30,75,113)"
    })
    await this.getOrder(this.state.filter)
    this.setState({
      switch: false
    })
    console.log(this.state.filter)
  }
  async setBulkorder() {
    this.setState({
      switch: true
    })
    await this.setState({
      filter: "bulkorder",
      orderBg: "#fff",
      bulkorderBg: "rgb(30,75,113)",
      bulkorderText: "#fff",
      orderText: "rgb(30,75,113)"
    })
    await this.getOrder(this.state.filter)
    console.log(this.state.filter)
    this.setState({
      switch: false
    })
  }

  generatePdf(rowData) {
    this.pauseAudio("")

    Share.share(
      {
        message: rowData.pdf,
        title: "7cs Gold"
      },
      {
        // // Android only:
        // dialogTitle: "Share BAM goodness",
        // // iOS only:
        // excludedActivityTypes: ["com.apple.UIKit.activity.PostToTwitter"]
      }
    )
  }

  previewPdf(rowData) {
    // console.warn("aiu")
    this.pauseAudio("")
    let result = WebBrowser.openBrowserAsync(rowData.pdf)
  }


  _onPlaybackStatusUpdate = status => {
    if (status.isLoaded) {
      if (status.didJustFinish && !status.isLooping) {
        this.pauseAudio("")
      }
    } else {
      if (status.error) {
        console.log(`FATAL PLAYER ERROR: ${status.error}`);
      }
    }
  };

  pauseAudio(rowData) {
    if (this.state.isAudioPlaying == true) {
      soundObject.pauseAsync()
      this.setState({
        isAudioPlaying: false,
        audioId: -999
      })
    }
  }

  playAudio(rowData) {

    if (rowData.audio) {
      this.setState({
        isAudioPlaying: true,
        audioId: rowData.order_id
      })


      try {
        const {sound, status} = new Audio.Sound.create(
          {
            uri: rowData.audio
          },
          {
            shouldPlay: true
          },
          this._onPlaybackStatusUpdate
        );

        this.soundObject = sound;
        console.log("Your sound is playing!")
      } catch (error) {
        // An error occurred!
        console.log(error)
        Alert.alert("Problem Occured", "No audio for order")
      }
    } else {
      Alert.alert("Problem Occured", "No audio for order")
    }
  }

  clearFilters() {
    this.searchInput.setNativeProps({
      text: ""
    })
    this.setState(
      {
        filterStartDate: "",
        filterEndDate: "",
        isSearchFilter: false,
        isEndDateTime: false,
        isStartDateTime: false
      },
      () => {
        this.getOrder(this.state.filter, "")
      }
    )
  }

  _showStartDateTimePicker = () => this.setState({
    isDateTimePickerVisible: true,
    isStartDateTime: true
  })

  _showEndDateTimePicker = () => this.setState({
    isDateTimePickerVisible: true,
    isEndDateTime: true
  })

  _hideDateTimePicker = () => this.setState({
    isDateTimePickerVisible: false
  })

  _handleDatePicked = date => {
    console.log("A date has been picked: ", date)
    var dateStr = date.toISOString().split("T")[0]
    if (this.state.isStartDateTime) {
      this.setState({
        filterStartDate: dateStr,
        isStartDateTime: false,
        minimumDate: date
      })
    }

    if (this.state.isEndDateTime) {
      this.setState({
        filterEndDate: dateStr,
        isEndDateTime: false,
        minimumDate: new Date("1970-01-01")
      })
    }
    this._hideDateTimePicker()
  }

  _hud: HudView

  render() {
    const {navigate} = this.props.navigation
    return (
      <View
      style={{
        flex: 1,
        paddingTop: 25
      }}
      >
        <Modal
      animationType="slide"
      transparent={false}
      visible={this.state.modalVisible}
      onRequestClose={() => {
        this.setModalVisible(!this.state.modalVisible)
      }}
      >
          <View
      style={{
        flex: 1,
        backgroundColor: "#000",
        justifyContent: "center"
      }}
      >
            <View
      style={{
        flexDirection: "column"
      }}
      >
              <TouchableHighlight
      onPress={() => this.setModalVisible(!this.state.modalVisible)}
      >
                <Text
      style={{
        fontSize: 20,
        marginTop: 40,
        marginLeft: 20,
        zIndex: 1,
        color: "#fff"
      }}
      >
                  X
                </Text>
              </TouchableHighlight>
              <Imagezoom
      style={styles.modalimg}
      source={{
        uri: this.state.modaluri
      }}
      />
            </View>
          </View>
        </Modal>

        <View
      style={{
        padding: 5,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center"
      }}
      >
          <Text
      style={{
        fontSize: 30,
        color: "rgb(30,75,113)"
      }}
      >
            7Cs Gold
          </Text>
        </View>

        <View
      style={{
        margin: 10
      }}
      >
          <View
      style={{
        marginTop: 5,
        flexDirection: "row",
        height: 50,
        padding: 10
      }}
      >
            <TouchableHighlight
      onPress={() => this.setOrder()}
      style={{
        backgroundColor: this.state.orderBg,
        height: 50,
        borderWidth: 1,
        borderColor: "#fff",
        borderRightWidth: 1,
        borderRightColor: "rgb(30,75,113)",
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center"
      }}
      >
              <Text
      style={{
        fontSize: 16,
        color: this.state.orderText
      }}
      >
                Order
              </Text>
            </TouchableHighlight>

            <TouchableHighlight
      onPress={() => this.setBulkorder()}
      style={{
        backgroundColor: this.state.bulkorderBg,
        height: 50,
        borderWidth: 1,
        borderColor: "#fff",
        borderLeftWidth: 1,
        borderLeftColor: "rgb(30,75,113)",
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center"
      }}
      >
              <Text
      style={{
        fontSize: 16,
        color: this.state.bulkorderText
      }}
      >
                Bulk Order
              </Text>
            </TouchableHighlight>
          </View>

          <View
      style={{
        top: 0,
        paddingTop: 20,
        paddingBottom: 0,
        paddingLeft: 10,
        paddingRight: 10,
        flexDirection: "row"
      }}
      >
            <Ionicons
      name="calendar"
      size={25}
      color="#2A4A6E"
      style={{
        padding: 10
      }}
      onPress={() => {
        this.popupDialog.show()
      }}
      />
            <TextInput
      ref={ref => (this.searchInput = ref)}
      style={{
        height: 40,
        borderColor: "gray",
        borderWidth: 1,
        flex: 0.8,
        backgroundColor: "#fff"
      }}
      onSubmitEditing={event => Keyboard.dismiss()}
      onEndEditing={event => {
        if (event.nativeEvent.text.length <= 0) {
        } else {
          this.setState({
            isSearchFilter: true
          })
        }
        this.getOrder(this.state.filter, event.nativeEvent.text)
      }}
      placeholder="  Order Number"
      multiline={false}
      onChangeText={text => {
        if (text.length <= 0) {
          this.getOrder(this.state.filter, "")
        }
      }}
      returnKeyType="search"
      />
            <Ionicons
      name="search"
      size={25}
      color="#2A4A6E"
      style={{
        padding: 10
      }}
      onPress={() => Keyboard.dismiss()}
      />
          </View>
          <View
      style={{
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 10,
        alignItems: "center",
        flexDirection: "row"
      }}
      >
            {this.state.isSearchFilter ? (
        <Text
        style={{
          flex: 1,
          textAlign: "center"
        }}
        onPress={() => this.clearFilters()}
        >
                Clear Filters
              </Text>
        ) : (
        <Text
        style={{
          flex: 1,
          textAlign: "center"
        }}
        />
        )}
          </View>

          <ListView
      removeClippedSubviews={false}
      refreshControl={
      <RefreshControl
      refreshing={this.state.refreshing}
      onRefresh={this._onRefresh.bind(this)}
      />
      }
      style={{
        marginBottom: 190
      }}
      dataSource={this.state.clonelist}
      renderRow={rowData => (
        <View
        style={{
          flexDirection: "row",
          alignSelf: "stretch",
          paddingVertical: 20,
          paddingHorizontal: 30,
          marginBottom: 1,
          backgroundColor: "#fff"
        }}
        >
                {this.state.filter != "order" ? (
          <View />
          ) : (
          <Image
          source={{
            uri: rowData.img_url
          }}
          style={{
            width: 100
          }}
          />
          )}
                <View
        style={{
          flex: 1,
          width: 100,
          marginLeft: 20,
          justifyContent: "center"
        }}
        >
                  <Text
        style={{
          fontSize: 16,
          marginBottom: 10
        }}
        >
                    {" "}
                    # {rowData.order_id}{" "}
                  </Text>
                  <Text
        style={{
          fontSize: 16,
          marginBottom: 10,
          color: "rgb(30,75,113)"
        }}
        >
                    {rowData.status}{" "}
                  </Text>
                  <Text
        style={{
          fontSize: 16,
          marginBottom: 10
        }}
        >
                    {rowData.date}{" "}
                  </Text>

                  <Text
        style={{
          fontSize: 16,
          marginBottom: 10
        }}
        >
                    Remarks: {rowData.remarks}
                  </Text>
                  <View
        style={{
          flexDirection: "row"
        }}
        >
                    {this.state.filter == "order" ? (
          <View />
          ) : (
          <TouchableHighlight
          underlayColor={"#fff"}
          style={{
            alignItems: "center"
          }}
          onPress={() => navigate("Bulk", {
            ref_id: rowData.ref_id
          })
          }
          >
                        <Ionicons
          style={{
            alignSelf: "center",
            padding: 10
          }}
          name="eye"
          size={24}
          color="rgb(30,75,113)"
          />
                      </TouchableHighlight>
          )}

                    <Ionicons
        style={{
          alignSelf: "center",
          padding: 10
        }}
        name="share-alt-square"
        size={24}
        color="rgb(30,75,113)"
        onPress={() => this.generatePdf(rowData)}
        />
                    <Ionicons
        style={{
          alignSelf: "center",
          padding: 10
        }}
        name="file"
        size={24}
        color="rgb(30,75,113)"
        onPress={() => this.previewPdf(rowData)}
        />
        {(this.state.isAudioPlaying == true && this.state.audioId == rowData.order_id) ?
          <Ionicons
          style={{
            alignSelf: "center",
            padding: 10
          }}
          name="pause"
          size={24}
          color="rgb(30,75,113)"
          onPress={() => this.pauseAudio(rowData)}
          />

          :

          <Ionicons
          style={{
            alignSelf: "center",
            padding: 10
          }}
          name="play"
          size={24}
          color="rgb(30,75,113)"
          onPress={() => this.playAudio(rowData)}
          />

        }
                  </View>
                </View>
              </View>
      )}
      enableEmptySections={true}
      />
        </View>

        <HudView
      ref={hud => {
        this._hud = hud
      }}
      delay={1.5}
      />

        <DateTimePicker
      isVisible={this.state.isDateTimePickerVisible}
      onConfirm={this._handleDatePicked}
      onCancel={this._hideDateTimePicker}
      minimumDate={this.state.minimumDate}
      maximumDate={new Date()}
      />
        <PopupDialog
      dialogTitle={<DialogTitle title="Select Date" />}
      ref={popupDialog => {
        this.popupDialog = popupDialog
      }}
      style={{
        padding: 20
      }}
      width={0.7}
      height={200}
      actions={[
        <DialogButton
        text="Search"
        onPress={() => {
          this.setState({
            isSearchFilter: true
          })
          this.popupDialog.dismiss(() => {
            console.log("callback - will be called immediately")
          })
          this.getOrder(this.state.filter, "")
        }}
        key="button-1"
        />
      ]}
      >
          <View>
            <Ionicons
      name="calendar"
      size={25}
      color="#2A4A6E"
      style={{
        padding: 10
      }}
      onPress={this._showStartDateTimePicker}
      >
              <Text
      style={{
        padding: 10,
        fontSize: 15
      }}
      >
                {this.state.filterStartDate.length > 0
        ? "  " + this.state.filterStartDate
        : "  Start Date"}
              </Text>
            </Ionicons>
            <Ionicons
      name="calendar"
      size={25}
      color="#2A4A6E"
      style={{
        padding: 10
      }}
      onPress={this._showEndDateTimePicker}
      >
              <Text
      style={{
        padding: 10,
        fontSize: 15
      }}
      >
                {this.state.filterEndDate.length > 0
        ? "  " + this.state.filterEndDate
        : "  End Date"}
              </Text>
            </Ionicons>
          </View>
        </PopupDialog>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "#F5FCFF",
    padding: 10,
    paddingTop: 80
  },
  input: {
    height: 50,
    width: 300,
    marginTop: 10,
    padding: 4,
    fontSize: 18,
    borderWidth: 1,
    borderColor: "#48bbec"
  },
  button: {
    height: 50,
    backgroundColor: "rgb(30,75,113)",
    alignSelf: "stretch",
    marginTop: 10,
    padding: 15,
    justifyContent: "center"
  },
  buttonText: {
    fontSize: 22,
    color: "#FFF",
    alignSelf: "center"
  },
  heading: {
    fontSize: 30
  },
  error: {
    color: "red",
    paddingTop: 10
  },
  loader: {
    marginTop: 20
  },
  stretch: {
    width: 150,
    height: 200
  },
  modalimg: {
    width: width,
    height: height - 20,
    zIndex: 0,
    alignSelf: "center",
    justifyContent: "center"
  },
  fitImage: {
    borderRadius: 20
  },
  fitImageWithSize: {
    height: 100,
    width: 30
  }
})
