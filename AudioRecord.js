/**
 * @flow
 */

import React from "react"
import {
  Dimensions,
  Image,
  Slider,
  StyleSheet,
  Text,
  TouchableHighlight,
  View
} from "react-native"
import Expo, { Asset, Audio, FileSystem, Font, Permissions } from "expo"
import Ionicons from "react-native-vector-icons/FontAwesome"

export default class AudioRecord extends React.Component {
  constructor(props) {
    super(props)
    this.recording = null
    this.sound = null
    this.isSeeking = false
    this.shouldPlayAtEndOfSeek = false
    this.state = {
      haveRecordingPermissions: false,
      isLoading: false,
      isPlaybackAllowed: false,
      muted: false,
      soundPosition: null,
      soundDuration: null,
      recordingDuration: null,
      shouldPlay: false,
      isPlaying: false,
      isRecording: false,
      fontLoaded: false,
      shouldCorrectPitch: true,
      volume: 1.0,
      rate: 1.0
    }
    this.recordingSettings = JSON.parse(
      JSON.stringify(Audio.RECORDING_OPTIONS_PRESET_LOW_QUALITY)
    )
  }

  componentDidMount() {
    // (async () => {
    //   await Font.loadAsync({
    //     'cutive-mono-regular': require('./assets/fonts/CutiveMono-Regular.ttf'),
    //   });
    //   this.setState({ fontLoaded: true });
    // })();
    this._askForPermissions()
  }

  componentDidUnMount() {
    this.setState({
      haveRecordingPermissions: false,
      isLoading: false,
      isPlaybackAllowed: false,
      muted: false,
      soundPosition: null,
      soundDuration: null,
      recordingDuration: null,
      shouldPlay: false,
      isPlaying: false,
      isRecording: false,
      fontLoaded: false,
      shouldCorrectPitch: true,
      volume: 1.0,
      rate: 1.0
    })
  }

  _askForPermissions = async () => {
    const response = await Permissions.askAsync(Permissions.AUDIO_RECORDING)
    this.setState({
      haveRecordingPermissions: response.status === "granted"
    })
  }

  // _updateScreenForSoundStatus = status => {
  //   if (status.isLoaded) {
  //     this.setState({
  //       soundDuration: status.durationMillis,
  //       soundPosition: status.positionMillis,

  //       isPlaying: status.isPlaying,

  //      // shouldCorrectPitch: status.shouldCorrectPitch,
  //       isPlaybackAllowed: true,
  //     });
  //   } else {
  //     this.setState({
  //       soundDuration: null,
  //       soundPosition: null,
  //       isPlaybackAllowed: false,
  //     });
  //     if (status.error) {
  //       console.log(`FATAL PLAYER ERROR: ${status.error}`);
  //     }
  //   }
  // };

  //recordign function
  _updateScreenForRecordingStatus = status => {
    if (status.canRecord) {
      this.setState({
        isRecording: status.isRecording,
        recordingDuration: status.durationMillis
      })
    } else if (status.isDoneRecording) {
      this.setState({
        isRecording: false,
        recordingDuration: status.durationMillis
      })
    }
  }

  //recording function
  async _stopPlaybackAndBeginRecording() {
    this.setState({
      isLoading: true
    })
    if (this.sound !== null) {
      await this.sound.unloadAsync()
      this.sound.setCallback(null)
      this.sound = null
    }
    await Audio.setAudioModeAsync({
      allowsRecordingIOS: true,
      interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
      playsInSilentModeIOS: true,
      shouldDuckAndroid: true,
      interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX
    })
    if (this.recording !== null) {
      this.recording.setCallback(null)
      this.recording = null
    }

    const recording = new Audio.Recording()
    await recording.prepareToRecordAsync(
      Audio.RECORDING_OPTIONS_PRESET_HIGH_QUALITY
    )
    recording.setCallback(this._updateScreenForRecordingStatus)

    this.recording = recording
    await this.recording.startAsync() // Will call callback to update the screen.
    this.setState({
      isLoading: false
    })
  }

  //recording function

  async _stopRecordingAndEnablePlayback() {
    this.setState({
      isLoading: true
    })
    try {
      await this.recording.stopAndUnloadAsync()
    } catch (error) {
      // Do nothing -- we are already unloaded.
    }
    const info = await FileSystem.getInfoAsync(this.recording.getURI())
    console.log(`FILE INFO: ${JSON.stringify(info)}`)
    this.props.url(info.uri)
    await Audio.setAudioModeAsync({
      allowsRecordingIOS: false,
      interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
      playsInSilentModeIOS: true,
      playsInSilentLockedModeIOS: true,
      shouldDuckAndroid: true,
      interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX
    })
    const { sound, status } = await this.recording.createNewLoadedSound(
      {
        isLooping: true
        // isMuted: this.state.muted,
        // volume: this.state.volume,
        //  rate: this.state.rate,
        //  shouldCorrectPitch: this.state.shouldCorrectPitch,
      }
      // this._updateScreenForSoundStatus
    )
    this.sound = sound

    this.setState({
      isLoading: false,
      isPlaybackAllowed: true,
      isPlaying: false
    })
  }

  //recording function
  _onRecordPressed = () => {
    if (this.state.isRecording) {
      this._stopRecordingAndEnablePlayback()
    } else {
      this._stopPlaybackAndBeginRecording()
    }
  }

  _onRefresh = () => {
    this.setState({
      isPlaying: false,
      isPlaybackAllowed: false
    })
    if (this.state.isRecording) {
      this._stopRecordingAndEnablePlayback()
    } else {
      this._stopPlaybackAndBeginRecording()
    }
  }

  _onPlayPausePressed = () => {
    if (this.sound != null) {
      if (this.state.isPlaying) {
        this.sound.pauseAsync()
        this.setState({
          isPlaying: false
        })
      } else {
        this.sound.playAsync()
        this.setState({
          isPlaying: true
        })
      }
    }
  }

  _onStopPressed = () => {
    if (this.sound != null) {
      this.sound.stopAsync()
    }
  }

  _onDelete = () => {
    if (this.sound != null) {
      this.setState({
        isLoading: false,
        isPlaybackAllowed: false,
        muted: false,
        soundPosition: null,
        soundDuration: null,
        recordingDuration: null,
        shouldPlay: false,
        isPlaying: false,
        isRecording: false,
        fontLoaded: false
      })
    }
  }

  // _onMutePressed = () => {
  //   if (this.sound != null) {
  //     this.sound.setIsMutedAsync(!this.state.muted);
  //   }
  // };

  // _onVolumeSliderValueChange = value => {
  //   if (this.sound != null) {
  //     this.sound.setVolumeAsync(value);
  //   }
  // };

  // _trySetRate = async (rate, shouldCorrectPitch) => {
  //   if (this.sound != null) {
  //     try {
  //       await this.sound.setRateAsync(rate, shouldCorrectPitch);
  //     } catch (error) {
  //       // Rate changing could not be performed, possibly because the client's Android API is too old.
  //     }
  //   }
  // };

  // _onRateSliderSlidingComplete = async value => {
  //   this._trySetRate(value * RATE_SCALE, this.state.shouldCorrectPitch);
  // };

  // _onPitchCorrectionPressed = async value => {
  //   this._trySetRate(this.state.rate, !this.state.shouldCorrectPitch);
  // };

  // _onSeekSliderValueChange = value => {
  //   if (this.sound != null && !this.isSeeking) {
  //     this.isSeeking = true;
  //     this.shouldPlayAtEndOfSeek = this.state.shouldPlay;
  //     this.sound.pauseAsync();
  //   }
  // };

  // _onSeekSliderSlidingComplete = async value => {
  //   if (this.sound != null) {
  //     this.isSeeking = false;
  //     const seekPosition = value * this.state.soundDuration;
  //     if (this.shouldPlayAtEndOfSeek) {
  //       this.sound.playFromPositionAsync(seekPosition);
  //     } else {
  //       this.sound.setPositionAsync(seekPosition);
  //     }
  //   }
  // };

  // _getSeekSliderPosition() {
  //   if (
  //     this.sound != null &&
  //     this.state.soundPosition != null &&
  //     this.state.soundDuration != null
  //   ) {
  //     return this.state.soundPosition / this.state.soundDuration;
  //   }
  //   return 0;
  // }

  _getMMSSFromMillis(millis) {
    const totalSeconds = millis / 1000
    const seconds = Math.floor(totalSeconds % 60)
    const minutes = Math.floor(totalSeconds / 60)

    const padWithZero = number => {
      const string = number.toString()
      if (number < 10) {
        return "0" + string
      }
      return string
    }
    return padWithZero(minutes) + ":" + padWithZero(seconds)
  }

  _getPlaybackTimestamp() {
    if (
      this.sound != null &&
      this.state.soundPosition != null &&
      this.state.soundDuration != null
    ) {
      return `${this._getMMSSFromMillis(
        this.state.soundPosition
      )} / ${this._getMMSSFromMillis(this.state.soundDuration)}`
    }
    return ""
  }

  _getRecordingTimestamp() {
    if (this.state.recordingDuration != null) {
      return `${this._getMMSSFromMillis(this.state.recordingDuration)}`
    }
    return `${this._getMMSSFromMillis(0)}`
  }

  render() {
    return !this.state.haveRecordingPermissions ? (
      <View style={styles.container}>
        <View />
        <Text>
          You must enable audio recording permissions in order to use this
          AudioRecord.
        </Text>
        <View />
      </View>
    ) : (
      <View>
        {!this.state.isPlaybackAllowed || this.state.isLoading ? (
          <View>
            <TouchableHighlight
              underlayColor={"#FFF"}
              onPress={this._onRecordPressed}
              disabled={this.state.isLoading}
            >
              {this.state.isRecording ? (
                <Ionicons
                  style={{
                    alignSelf: "center",
                    padding: 20,
                    borderRadius: 100,
                    backgroundColor: "rgb(30,75,113)",
                    marginBottom: 15
                  }}
                  name="stop"
                  size={24}
                  color="#fff"
                />
              ) : (
                <Ionicons
                  style={{
                    alignSelf: "center",
                    padding: 20,
                    borderRadius: 100,
                    backgroundColor: "rgb(30,75,113)",
                    marginBottom: 15
                  }}
                  name="microphone"
                  size={24}
                  color="#fff"
                />
              )}
            </TouchableHighlight>
            <Text
              style={{
                alignSelf: "center"
              }}
            >
              {this._getRecordingTimestamp()}
            </Text>
          </View>
        ) : (
          <View>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "center"
              }}
            >
              <TouchableHighlight
                underlayColor={"#fafafa"}
                style={{
                  marginRight: 10
                }}
                onPress={this._onPlayPausePressed}
                disabled={!this.state.isPlaybackAllowed || this.state.isLoading}
              >
                {this.state.isPlaying ? (
                  <Ionicons
                    style={{
                      alignSelf: "center",
                      padding: 20,
                      borderRadius: 100,
                      backgroundColor: "rgb(30,75,113)",
                      marginBottom: 15
                    }}
                    name="pause"
                    size={24}
                    color="#fff"
                  />
                ) : (
                  <Ionicons
                    style={{
                      alignSelf: "center",
                      padding: 20,
                      borderRadius: 100,
                      backgroundColor: "rgb(30,75,113)",
                      marginBottom: 15
                    }}
                    name="play"
                    size={24}
                    color="#fff"
                  />
                )}
              </TouchableHighlight>

              <TouchableHighlight
                underlayColor={"#fafafa"}
                style={{
                  marginLeft: 10
                }}
                onPress={this._onRefresh}
              >
                <Ionicons
                  style={{
                    alignSelf: "center",
                    padding: 20,
                    borderRadius: 100,
                    backgroundColor: "rgb(30,75,113)",
                    marginBottom: 15
                  }}
                  name="refresh"
                  size={24}
                  color="#fff"
                />
              </TouchableHighlight>
              <TouchableHighlight
                underlayColor={"#fafafa"}
                style={{
                  marginLeft: 10
                }}
                onPress={this._onDelete}
              >
                <Ionicons
                  style={{
                    alignSelf: "center",
                    padding: 20,
                    borderRadius: 100,
                    backgroundColor: "rgb(30,75,113)",
                    marginBottom: 15
                  }}
                  name="trash"
                  size={24}
                  color="red"
                />
              </TouchableHighlight>
            </View>
          </View>
        )}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  emptyContainer: {
    alignSelf: "stretch"
  },
  container: {
    flex: 1,

    alignItems: "center",
    alignSelf: "stretch"
  },
  noPermissionsText: {
    textAlign: "center"
  },
  wrAudioRecorder: {},
  halfScreenContainer: {}
})
