import React, { Component } from "react"

import {
  Text,
  TouchableHighlight,
  View,
  TextInput,
  StyleSheet,
  Picker,
  Alert,
  AsyncStorage
} from "react-native"
import AudioRecord from "./AudioRecord"
import Modal from "react-native-modal"
import md5 from "react-native-md5"
import HudView from "react-native-easy-hud"

const ACCESS_TOKEN = "access_token"
const USER_ID = "user_id"

class ModalPanel extends Component {
  constructor(props) {
    super(props)

    this.state = {
      uid: "",
      pid: props.pid,
      qty: props.qty,
      karatage: "22",
      remarks: "",
      modalVisible: false,
      success: "",
      errors: [],
      audio: ""
    }
  }

  async setSession() {
    try {
      const user_id = await AsyncStorage.getItem(USER_ID)
      if (user_id !== null) {
        // We have data!!
        console.log("Set Session hello")
        console.log(user_id)

        this.setState({ uid: user_id })
      }
    } catch (error) {
      console.log(error)
      // Error retrieving data
    }
  }

  async componentDidMount() {
    // console.log("hello");
    await this.setSession()
  }

  async placeOrder() {
    let rand = md5.b64_md5(Date.now() + this.state.uid)
    let audio = rand + ".m4a"

    this.setState({ loading: true })
    var data = new FormData()
    data.append("audio", {
      uri: this.state.audio,
      name: audio,
      type: "audio/m4a"
    })
    data.append("user_id", this.state.uid)
    data.append("product_id", this.state.pid)
    data.append("qty", this.state.qty)
    data.append("karatage", this.state.karatage)
    data.append("remarks", this.state.remarks)
    console.log(data)
    const config = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "multipart/form-data"
      },
      body: data
    }

    try {
      let resp = await fetch(
        "http://b2bapp.7csgold.com/api/customer_post_order.php",
        config
      )
    } catch (error) {
      console.log(error)
    }

    let response = await fetch(
      "http://b2bapp.7csgold.com/api/customer_post_order.php",
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          user_id: this.state.uid,
          product_id: this.state.pid,
          qty: this.state.qty,
          karatage: this.state.karatage,
          remarks: this.state.remarks,
          audio: audio
        })
      }
    )
    let res = await response.json()

    if (response.status >= 200 && response.status < 300) {
      //Handle success
      if (res.status) {
        let status = res.status

        if (status) {
          console.log(status)

          Alert.alert("Order Placed", "Thankyou for placing the order.")
          this.toggleModal(false)
          // const resetAction = NavigationActions.reset({
          //                   index: 0,
          //                   actions: [NavigationActions.navigate({ routeName: 'Home' })],
          //                 });

          //                 this.props.navigation.dispatch(resetAction);
        } else {
          Alert.alert("Problem Occured", "Please Try Again")
        }
      } else {
        Alert.alert("Problem Occured", "Please Try Again")
      }

      //this.redirect('home');
    } else {
      //Handle error
      let error = res
      throw error
      Alert.alert("Problem Occured", "Please Try Again")
    }
  }

  toggleModal(visible) {
    this.setState({ modalVisible: visible })
  }

  setAudioUrl = data => {
    console.log(data)
    this.setState({ audio: data })
  }

  _hud: HudView

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Modal
          animationIn={"slideInUp"}
          isVisible={this.state.modalVisible}
          onBackdropPress={() => {
            this.toggleModal(!this.state.modalVisible)
          }}
        >
          <View style={styles.modal}>
            <View style={{ flexDirection: "row", alignSelf: "stretch" }}>
              <Text style={{ fontSize: 16, color: "rgb(30,75,113)", flex: 2 }}>
                Karatage
              </Text>
              <Text style={{ fontSize: 16, color: "rgb(30,75,113)", flex: 1 }}>
                :
              </Text>

              <Picker
                selectedValue={this.state.karatage}
                onValueChange={karatage =>
                  this.setState({ karatage: karatage })
                }
                mode="dropdown"
                style={{
                  width: 150,
                  color: "black",
                  borderWidth: 1,
                  borderColor: "#fff",
                  borderBottomWidth: 1,
                  borderBottomColor: "rgb(30,75,113)",
                  bottom: 12
                }}
              >
                <Picker.Item label="22" value="22" />
                <Picker.Item label="21" value="21" />
                <Picker.Item label="18" value="18" />
              </Picker>
            </View>

            <View style={{ flexDirection: "row", alignSelf: "stretch" }}>
              <Text style={{ fontSize: 16, color: "rgb(30,75,113)", flex: 2 }}>
                Size/Weight
              </Text>
              <Text style={{ fontSize: 16, color: "rgb(30,75,113)", flex: 1 }}>
                :
              </Text>

              <TextInput
                style={styles.input}
                onChangeText={text => this.setState({ qty: text })}
                placeholder="Size/Weight/Qty"
              />
            </View>

            <View style={{ flexDirection: "row", alignSelf: "stretch" }}>
              <Text style={{ fontSize: 16, color: "rgb(30,75,113)", flex: 2 }}>
                Remarks
              </Text>
              <Text style={{ fontSize: 16, color: "rgb(30,75,113)", flex: 1 }}>
                :
              </Text>

              <TextInput
                style={styles.inputBig}
                multiline={true}
                numberOfLines={4}
                onChangeText={text => this.setState({ remarks: text })}
                placeholder="Remarks"
              />
            </View>
            <AudioRecord url={this.setAudioUrl} />

            <View
              style={{ marginBottom: 10, flexDirection: "row", height: 60 }}
            >
              <TouchableHighlight
                underlayColor={"#fff"}
                style={{
                  flex: 1,
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "center"
                }}
                onPress={() => {
                  this.placeOrder()
                }}
              >
                <Text style={styles.modalText}>Order </Text>
              </TouchableHighlight>
            </View>
          </View>
        </Modal>

        <TouchableHighlight
          style={styles.container}
          onPress={() => {
            this.toggleModal(true)
          }}
        >
          <Text style={styles.text}>Order</Text>
        </TouchableHighlight>
        <HudView
          ref={hud => {
            this._hud = hud
          }}
          delay={1.5}
        />
      </View>
    )
  }
}

export default ModalPanel

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignSelf: "center",
    padding: 10
  },
  modal: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
    paddingHorizontal: 20,
    paddingVertical: 30
  },
  input: {
    width: 150,
    padding: 10,
    bottom: 12
  },
  inputBig: {
    width: 150,
    padding: 10,
    bottom: 12,
    height: 70
  },
  modalText: {
    color: "#fff",
    borderRadius: 40,
    width: 150,
    paddingVertical: 10,
    fontSize: 22,
    textAlign: "center",
    borderWidth: 1,
    borderColor: "rgb(30,75,113)",
    backgroundColor: "rgb(30,75,113)"
  },
  text: {
    color: "#fff",
    borderRadius: 40,
    width: 150,
    padding: 5,
    fontSize: 18,
    textAlign: "center",
    borderWidth: 1,
    borderColor: "rgb(30,75,113)",
    backgroundColor: "rgb(30,75,113)"
  }
})
