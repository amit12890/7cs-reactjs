import React, { Component } from "react"
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableHighlight,
  Alert
} from "react-native"
import Ionicons from "react-native-vector-icons/FontAwesome"
import HudView from "react-native-easy-hud"
export default class Reset extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: ""
    }
  }

  async forgotPassword() {
    console.log(this.state.email)
    try {
      // this.redireu

      let response = await fetch(
        "http://b2bapp.7csgold.com/api/reset_password.php",
        {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            email: this.state.email
          })
        }
      )
      let res = await response.json()
      if (response.status >= 200 && response.status < 300) {
        if (res.status) {
          Alert.alert("Thank you", "Please Check your Email")
        } else {
          Alert.alert("Something went wrong", "Try again after sometime")
        }

        // this.redirect('root');
      } else {
        Alert.alert("Something went wrong", "Try again after sometime")
      }
    } catch (error) {
      //this.redirect('login');
      Alert.alert("Something went wrong", "Try again after sometime")
    }
  }

  _hud: HudView

  render() {
    const { navigate } = this.props.navigation
    return (
      <View style={{ flex: 1 }}>
        <View style={{ backgroundColor: "#fff", padding: 10, margin: 10 }}>
          <TextInput
            style={{ padding: 10 }}
            onChangeText={text => this.setState({ email: text })}
            placeholder="Enter Your Email Address"
          />
          <TouchableHighlight
            style={styles.mbutton}
            onPress={() => this.forgotPassword()}
          >
            <Text style={styles.mbuttonText}>Submit</Text>
          </TouchableHighlight>
        </View>
        <HudView
          ref={hud => {
            this._hud = hud
          }}
          delay={1.5}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  mbutton: {
    height: 50,
    backgroundColor: "rgb(30,75,113)",
    borderRadius: 50,

    alignSelf: "stretch",
    marginTop: 10,
    marginBottom: 10,
    justifyContent: "center"
  },
  mbuttonText: {
    fontSize: 18,
    color: "#FFF",
    alignSelf: "center"
  }
})
