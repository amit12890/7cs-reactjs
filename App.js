import React from "react"
import { StackNavigator } from "react-navigation"
import { StatusBar } from 'react-native';
import LoginScreen from "./Login"
import AudioRecordScreen from "./AudioRecord"
import TestScreen from "./Test"
import HomeScreen from "./Home"
import MoreScreen from "./More"
import RegisterScreen from "./Register"
import ThankyouScreen from "./Thankyou"
import ListScreen from "./List"
import LatestScreen from "./Latest"
import TopsellingScreen from "./Topselling"
import VendorScreen from "./Vendor"
import UserPreferScreen from "./UserPrefer"
import EmployeeScreen from "./Employee"
import BulkScreen from "./Bulk"
import ContactScreen from "./Contact"
import AboutScreen from "./About"
import TermsScreen from "./Terms"
import PrivacyScreen from "./Privacy"
import ResetScreen from "./Reset"

StatusBar.setHidden(true);

const AppNavigation = StackNavigator({
  Login: {
    screen: LoginScreen,
    navigationOptions: {
      title: "Login Screen",
      header: null
    }
  },
  Reset: {
    screen: ResetScreen,
    navigationOptions: {
      title: "Forgot Password",
      headerStyle: {}
    }
  },
  Contact: {
    screen: ContactScreen,
    navigationOptions: {
      title: "Contact Us",
      headerStyle: {}
    }
  },
  About: {
    screen: AboutScreen,
    navigationOptions: {
      title: "About Us",
      headerStyle: {}
    }
  },
  Terms: {
    screen: TermsScreen,
    navigationOptions: {
      title: "Terms and Conditions",
      headerStyle: {}
    }
  },
  Privacy: {
    screen: PrivacyScreen,
    navigationOptions: {
      title: "Privacy Policy",
      headerStyle: {}
    }
  },
  More: {
    screen: MoreScreen,
    navigationOptions: {
      title: "Show More Screen",
      header: null
    }
  },

  Bulk: {
    screen: BulkScreen,
    navigationOptions: {
      title: "Bulk Order",
      headerStyle: {}
    }
  },

  UserPrefer: {
    screen: UserPreferScreen,
    navigationOptions: {
      title: "User Preferences",
      headerStyle: {}
    }
  },
  Employee: {
    screen: EmployeeScreen,
    navigationOptions: {
      title: "Employee Screen",
      header: null
    }
  },
  Home: {
    screen: HomeScreen,
    navigationOptions: {
      title: "Home Screen",
      header: null
    }
  },
  Vendor: {
    screen: VendorScreen,
    navigationOptions: {
      title: "Vendor Screen",
      header: null
    }
  },
  Register: {
    screen: RegisterScreen,
    navigationOptions: {
      headerStyle: {}
    }
  },
  List: {
    screen: ListScreen,
    navigationOptions: {
      headerStyle: {}
    }
  },
  Latest: {
    screen: LatestScreen,
    navigationOptions: {
      headerStyle: {}
    }
  },
  Topselling: {
    screen: TopsellingScreen,
    navigationOptions: {
      headerStyle: {}
    }
  },
  Thankyou: {
    screen: ThankyouScreen,
    navigationOptions: {
      title: "Thankyou Screen",
      header: null
    }
  }
})

export default AppNavigation
