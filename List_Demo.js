import React, { Component } from "react"
import {
  View,
  Text,
  StyleSheet,
  Button,
  Image,
  ListView,
  AsyncStorage,
  TextInput,
  ActivityIndicator,
  TouchableHighlight,
  RefreshControl,
  ScrollView,
  Keyboard,
  Alert,
  Picker,
  Modal
} from "react-native"

import AudioRecord from "./AudioRecord"
import Icon from "react-native-vector-icons/FontAwesome"
import FAB from "react-native-fab"
import GridView from "react-native-easy-gridview"
import { NavigationActions } from "react-navigation"
import Dimensions from "Dimensions"
const { width, height } = Dimensions.get("window")
import HudView from "react-native-easy-hud"
import { Checkbox, CheckboxGroup } from "react-native-material-design"

import LikeButton from "./LikeButton"
import ModalPanel from "./ModalPanel"
import Imagezoom from "react-native-transformable-image"
import Ionicons from "react-native-vector-icons/FontAwesome"
import Maticons from "react-native-vector-icons/Entypo"
import md5 from "react-native-md5"
//import Upload from './Upload';

const ACCESS_TOKEN = "access_token"
const USER_ID = "user_id"
let _this = null
export default class ListScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.state.params.cat,
    headerRight: (
      <TouchableHighlight
        style={{ marginRight: 20 }}
        onPress={() => _this.toggleFilterModal(this)}
      >
        <Icon name="filter" style={{ fontSize: 20 }} />
      </TouchableHighlight>
    )
  })

  constructor(props) {
    super(props)

    var ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
    var tds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
    this.state = {
      orderPlaced: false,
      selected: [],
      bulkModalVisible: false,
      refreshing: false,
      json: [],
      tempJson: [],
      checked: [],
      collection: [],
      karatage: "",
      qty: "",
      uid: "",
      email: "",
      modaluri: "",
      audio: "",
      accessToken: "",
      bottom: 0,
      type: "",
      remarks: "",
      modalVisible: false,
      filterModal: false,
      result: "",
      templist: tds.cloneWithRows([]),
      clonelist: ds.cloneWithRows([])
    }
  }

  async componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      this._keyboardDidShow.bind(this)
    )
    this.keyboardDidHideListener = Keyboard.addListener(
      "keyboardDidHide",
      this._keyboardDidHide.bind(this)
    )

    let url = "http://b2bapp.7csgold.com/api/get_collection.php"

    await fetch(url, {
      method: "GET"
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log(responseJson)
        this.setState({ collection: responseJson.collection })
      })
      .catch(error => {
        console.error(error)
      })
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove()
    this.keyboardDidHideListener.remove()
  }

  _keyboardDidShow() {
    // console.log(this.state.bottom);
    this.setState({ bottom: 130 })
  }

  _keyboardDidHide() {
    this.setState({ bottom: 0 })
  }

  toggleFilterModal(flag) {
    this.setState({ filterModal: flag })
  }

  toggleModal(visible) {
    // console.log(this.state.json.length);

    if (visible) {
      for (var i = this.state.json.length - 1; i >= 0; i--) {
        for (var j = this.state.selected.length - 1; j >= 0; j--) {
          if (this.state.selected[j] === this.state.json[i].id) {
            this.state.tempJson.push(this.state.json[i])
            break
          } else {
            continue
          }
        }
      }
      console.log(this.state.tempJson)
      var tds = new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2
      })
      this.setState({
        templist: tds.cloneWithRows(this.state.tempJson)
      })
      this.state.tempJson = []
    } else {
      var tds = new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2
      })
      this.setState({
        templist: tds.cloneWithRows([])
      })
    }

    this.setState({ bulkModalVisible: visible })
  }

  setModalVisible(visible, img) {
    console.log(img)
    this.setState({ modalVisible: visible })
    this.setState({ modaluri: img })
  }

  _onRefresh() {
    this.setState({ refreshing: true })
    let user_id = this.state.uid
    const { params } = this.props.navigation.state
    let cat = params.cat
    let filter = params.filter
    let url =
      "http://b2bapp.7csgold.com/api/customer_feed.php?id=" +
      user_id +
      "&cat=" +
      cat +
      "&filter=" +
      filter
    console.log(url)
    fetch(url, {
      method: "GET"
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log(responseJson)

        this.setState({
          json: responseJson.product
        })

        var ds = new ListView.DataSource({
          rowHasChanged: (r1, r2) => r1 !== r2
        })
        this.setState({
          clonelist: ds.cloneWithRows(responseJson.product)
        })
      })

      .catch(error => {
        console.error(error)
      })
    this.setState({ refreshing: false })
  }
  async setSession() {
    try {
      const value = await AsyncStorage.getItem(ACCESS_TOKEN)
      const user_id = await AsyncStorage.getItem(USER_ID)
      if (value !== null) {
        // We have data!!
        console.log("List Set Session")
        this.setState({ accessToken: value })
        this.setState({ uid: user_id })
      }
    } catch (error) {
      console.log(error)
      // Error retrieving data
    }
  }
  async storeToken(responseData) {
    console.log(responseData)
    try {
      await AsyncStorage.setItem(ACCESS_TOKEN, responseData)
    } catch (error) {
      console.log("storage error")
      // Error saving data
    }
  }

  _onChange = (checked, value) => {
    console.log("checking checked variable: " + checked)
    console.log("checking value variable: " + value)
    const { selected } = this.state
    console.log("checking selected state variable: " + selected)
    var newSelected
    if (checked) {
      newSelected = [...selected, value]
    } else {
      let index = selected.indexOf(value)
      newSelected = [...selected.slice(0, index), ...selected.slice(index + 1)]
    }

    console.log(newSelected)

    console.log(
      "checking selected index variable: " + this.state.selected.indexOf(value)
    )
    this.setState({
      selected: newSelected
    })
  }

  async setFilter(values) {
    console.log(values)
    this.setState({
      checked: values
    })

    if (values && values.length > 0) {
      for (var i = this.state.json.length - 1; i >= 0; i--) {
        for (var j = values.length - 1; j >= 0; j--) {
          if (values[j] === this.state.json[i].sub_category) {
            this.state.tempJson.push(this.state.json[i])
            break
          } else {
            continue
          }
        }
      }
      console.log(this.state.tempJson)
      var ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
      this.setState({
        clonelist: ds.cloneWithRows(this.state.tempJson)
      })
      this.state.tempJson = []
    } else {
      console.log(this.state.tempJson)
      var ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
      this.setState({
        clonelist: ds.cloneWithRows(this.state.json)
      })
      this.state.tempJson = []
    }
  }

  async getToken() {
    try {
      const value = await AsyncStorage.getItem(ACCESS_TOKEN)
      if (value !== null) {
        // We have data!!
        console.log("has storage")
        console.log(value)
      }
    } catch (error) {
      console.log(error)
      // Error retrieving data
    }
  }

  async componentDidMount() {
    // console.log("hello");
    _this = this
    await this.setSession()
    let user_id = this.state.uid
    const { params } = this.props.navigation.state
    let cat = params.cat
    let filter = params.filter
    let url =
      "http://b2bapp.7csgold.com/api/customer_feed.php?id=" +
      user_id +
      "&cat=" +
      cat +
      "&filter=" +
      filter
    console.log(url)
    await fetch(url, {
      method: "GET"
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log(responseJson)
        this.setState({
          json: responseJson.product
        })
        var ds = new ListView.DataSource({
          rowHasChanged: (r1, r2) => r1 !== r2
        })
        this.setState({
          clonelist: ds.cloneWithRows(responseJson.product)
        })
      })
      .catch(error => {
        console.error(error)
      })
  }

  async placeBulkOrder() {
    console.log("bulk order clicked")
    this.setState({ orderPlaced: true })
    // console.log(this.state.uid + " " + this.state.selected + " " + this.state.remarks);
    // let response = await fetch('http://b2bapp.7csgold.com/api/customer_bulk_order.php', {
    //                             method: 'POST',
    //                             headers: {
    //                               'Accept': 'application/json',
    //                               'Content-Type': 'application/json',
    //                             },
    //                             body: JSON.stringify({

    //                                 user_id: this.state.uid,
    //                                 product_id: this.state.selected,
    //                                 remarks: this.state.remarks

    //                             })
    //                           });
    //  let audio = new Date().toLocaleString() + this.state.uid + ".m4a";

    let rand = md5.b64_md5(Date.now() + this.state.uid)
    let audio = rand + ".m4a"

    this.setState({ loading: true })
    var data = new FormData()
    data.append("audio", {
      uri: this.state.audio,
      name: audio,
      type: "audio/m4a"
    })
    data.append("user_id", this.state.uid)
    data.append("product_id", this.state.selected)
    data.append("karatage", this.state.karatage)
    data.append("qty", this.state.qty)
    data.append("remarks", this.state.remarks)
    console.log(data)
    const config = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "multipart/form-data"
      },
      body: data
    }

    try {
      let resp = await fetch(
        "http://b2bapp.7csgold.com/api/customer_bulk_order.php",
        config
      )
    } catch (error) {
      console.log(error)
    }

    let response = await fetch(
      "http://b2bapp.7csgold.com/api/customer_bulk_order.php",
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          user_id: this.state.uid,
          product_id: this.state.selected,
          karatage: this.state.karatage,
          qty: this.state.qty,
          remarks: this.state.remarks,
          audio: audio
        })
      }
    )
    let res = await response.json()

    if (response.status >= 200 && response.status < 300) {
      //Handle success
      if (res.status) {
        let status = res.status

        if (status) {
          console.log(status)
          this.setState({ orderPlaced: false })
          Alert.alert("Order Placed", "Thankyou for placing the order.")

          this.toggleModal(false)
          const resetAction = NavigationActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: "Home" })]
          })

          this.props.navigation.dispatch(resetAction)
        } else {
          Alert.alert("Problem Occured", "Please Try Again")
        }
      } else {
        Alert.alert("Problem Occured", "Please Try Again")
      }

      //this.redirect('home');
    } else {
      //Handle error
      let error = res
      throw error
      Alert.alert("Problem Occured", "Please Try Again")
    }
  }

  setAudioUrl = data => {
    console.log(data)
    this.setState({ audio: data })
  }

  _hud: HudView

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Modal
          animationType={"slide"}
          transparent={false}
          visible={this.state.filterModal}
          onRequestClose={() => {
            this.toggleFilterModal(false)
          }}
        >
          <View style={{ flex: 1, margin: 20 }}>
            <ScrollView>
              <CheckboxGroup
                onSelect={values => {
                  this.setFilter(values)
                }}
                checked={this.state.checked}
                items={this.state.collection}
              />
            </ScrollView>
            <TouchableHighlight
              style={styles.mbutton}
              onPress={() => this.toggleFilterModal(false)}
            >
              <Text style={styles.mbuttonText}>Set Filter</Text>
            </TouchableHighlight>
          </View>
        </Modal>
        <Modal
          animationType={"slide"}
          transparent={false}
          visible={this.state.bulkModalVisible}
          onRequestClose={() => {
            this.toggleModal(false)
          }}
        >
          <View style={{ padding: 20, bottom: this.state.bottom }}>
            <View
              style={{
                padding: 5,
                backgroundColor: "#fff",
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <Text style={{ fontSize: 30, color: "rgb(30,75,113)" }}>
                7Cs Gold
              </Text>
            </View>
            <ScrollView style={{ marginBottom: 10 }}>
              <AudioRecord url={this.setAudioUrl} />
              <View style={{ flexDirection: "row", alignSelf: "stretch" }}>
                <Text
                  style={{ fontSize: 16, color: "rgb(30,75,113)", flex: 2 }}
                >
                  Karatage
                </Text>
                <Text
                  style={{ fontSize: 16, color: "rgb(30,75,113)", flex: 1 }}
                >
                  :
                </Text>

                <Picker
                  selectedValue={this.state.karatage}
                  onValueChange={karatage =>
                    this.setState({ karatage: karatage })
                  }
                  mode="dropdown"
                  style={{
                    width: 150,
                    color: "black",
                    borderWidth: 1,
                    borderColor: "#fff",
                    borderBottomWidth: 1,
                    borderBottomColor: "rgb(30,75,113)",
                    bottom: 12
                  }}
                >
                  <Picker.Item label="22" value="22" />
                  <Picker.Item label="21" value="21" />
                  <Picker.Item label="18" value="18" />
                </Picker>
              </View>

              <View style={{ flexDirection: "row", alignSelf: "stretch" }}>
                <TextInput
                  style={{ padding: 10, width: 300 }}
                  onChangeText={text => this.setState({ qty: text })}
                  placeholder="Size/Weight/Qty"
                />
              </View>
              <TextInput
                style={{ height: 70, padding: 10 }}
                multiline={true}
                numberOfLines={4}
                onChangeText={text => this.setState({ remarks: text })}
                value={this.state.remarks}
                placeholder="Remarks"
              />

              <GridView
                dataSource={this.state.templist}
                renderRow={rowData => (
                  <View
                    style={{
                      marginHorizontal: 10,
                      marginTop: 20,
                      padding: 10,
                      paddingTop: 20,
                      backgroundColor: "#fff"
                    }}
                  >
                    <Image
                      style={{ width: 100, height: 100 }}
                      source={{ uri: rowData.img_url }}
                    />
                  </View>
                )}
                numberOfItemsPerRow={2}
                removeClippedSubviews={false}
                initialListSize={1}
                pageSize={5}
              />
              {this.state.orderPlaced ? (
                <ActivityIndicator
                  animating={this.state.orderPlaced}
                  color="rgb(30,75,113)"
                  size="large"
                  style={styles.activityIndicator}
                />
              ) : (
                <TouchableHighlight
                  style={styles.mbutton}
                  onPress={() => this.placeBulkOrder()}
                >
                  <Text style={styles.mbuttonText}>Order</Text>
                </TouchableHighlight>
              )}
            </ScrollView>
          </View>
        </Modal>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            this.setModalVisible(!this.state.modalVisible)
          }}
        >
          <View
            style={{
              flex: 1,
              backgroundColor: "#000",
              justifyContent: "center"
            }}
          >
            <View style={{ flexDirection: "column" }}>
              <TouchableHighlight
                onPress={() => this.setModalVisible(!this.state.modalVisible)}
              >
                <Text
                  style={{
                    fontSize: 20,
                    marginTop: 40,
                    marginLeft: 20,
                    zIndex: 1,
                    color: "#fff"
                  }}
                >
                  X
                </Text>
              </TouchableHighlight>
              <Imagezoom
                style={styles.modalimg}
                source={{ uri: this.state.modaluri }}
              />
            </View>
          </View>
        </Modal>

        <ListView
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh.bind(this)}
            />
          }
          dataSource={this.state.clonelist}
          renderRow={rowData => (
            <View
              style={{
                marginHorizontal: 10,
                marginTop: 20,
                padding: 10,
                paddingTop: 20,
                backgroundColor: "#fff"
              }}
            >
              <Checkbox
                value={rowData.id}
                onCheck={this._onChange}
                checked={
                  this.state.selected &&
                  this.state.selected.indexOf(rowData.id) !== -1
                }
              />
              <TouchableHighlight
                underlayColor={"#fff"}
                onPress={() => this.setModalVisible(true, rowData.img_url)}
              >
                <Image
                  style={styles.stretch}
                  source={{ uri: rowData.img_url }}
                />
              </TouchableHighlight>

              <View
                style={{
                  flexDirection: "row",
                  marginHorizontal: 15,
                  marginTop: 10,
                  alignSelf: "center",
                  justifyContent: "space-between"
                }}
              >
                <Text
                  style={{ flex: 1, fontSize: 18, color: "rgb(30,75,113)" }}
                >
                  {rowData.karatage} kt{" "}
                </Text>
                <Text
                  style={{ flex: 1, fontSize: 18, color: "rgb(30,75,113)" }}
                >
                  {rowData.weight} gm{" "}
                </Text>
                <LikeButton
                  style={{ flex: 1, flexDirection: "row" }}
                  id={rowData.id}
                  liked={rowData.like}
                />
              </View>

              <Text
                style={{
                  alignSelf: "stretch",
                  marginHorizontal: 20,
                  color: "rgb(30,75,113)"
                }}
              >
                Remarks: {rowData.remarks}{" "}
              </Text>
              <View style={{ flexDirection: "row", alignSelf: "center" }}>
                <ModalPanel
                  pid={rowData.id}
                  style={{ flex: 1, flexDirection: "row" }}
                />
              </View>
            </View>
          )}
          enableEmptySections={true}
        />
        <FAB
          buttonColor="rgb(30,75,113)"
          iconTextColor="#FFFFFF"
          onClickAction={() => {
            this.toggleModal(true)
          }}
          visible={this.state.selected.length > 0}
          iconTextComponent={<Text> {this.state.selected.length} </Text>}
        />
        <HudView
          ref={hud => {
            this._hud = hud
          }}
          delay={1.5}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "#F5FCFF",
    padding: 10,
    paddingTop: 80
  },
  input: {
    height: 50,
    width: 300,
    marginTop: 10,
    padding: 4,
    fontSize: 18,
    borderWidth: 1,
    borderColor: "#48bbec"
  },
  button: {
    height: 50,
    backgroundColor: "#48BBEC",
    alignSelf: "stretch",
    marginTop: 30,
    justifyContent: "center",
    position: "absolute"
  },
  mbutton: {
    height: 50,
    backgroundColor: "rgb(30,75,113)",
    borderRadius: 50,

    alignSelf: "stretch",
    marginTop: 10,
    marginBottom: 10,
    justifyContent: "center"
  },
  mbuttonText: {
    fontSize: 18,
    color: "#FFF",
    alignSelf: "center"
  },
  buttonText: {
    fontSize: 22,
    color: "#FFF",
    alignSelf: "center"
  },
  heading: {
    fontSize: 30
  },
  error: {
    color: "red",
    paddingTop: 10
  },
  loader: {
    marginTop: 20
  },
  stretch: {
    width: 300,
    height: 300,
    alignSelf: "center"
  },
  modalimg: {
    width: width,
    height: height - 20,
    zIndex: 0,
    alignSelf: "center",
    justifyContent: "center"
  }
})
