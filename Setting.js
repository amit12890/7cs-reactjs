import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Button,
    Image,
  ListView,
  AsyncStorage,
  ActivityIndicator,
  TouchableHighlight
} from 'react-native';
import Ionicons from 'react-native-vector-icons/FontAwesome';

const ACCESS_TOKEN = 'access_token';
const TYPE = 'type';
const flag = true;

export default class Setting extends Component {

constructor(props)
{
  console.log("called");
    super(props);
    
    this.state = 
    {
         flag: true
    }
    
   
}
    
async componentDidMount(){
    const value = await AsyncStorage.getItem(TYPE);
    if(value === 'vendor')
        {
            
            this.setState({flag: false});
            console.log(flag);
        }
}
    
  static navigationOptions = {
    tabBarLabel: 'Settings',
   tabBarIcon: () => ( <Ionicons style={{alignSelf: 'center'}} name="gear" size={24} color="rgb(30,75,113)" />)
  }

async logout()
{
   
   try {
        await AsyncStorage.removeItem(ACCESS_TOKEN);
        await AsyncStorage.clear();
        const { navigate } = this.props.navigation;
        navigate('Login');
    } catch (error) {
        console.log('storage error');
        // Error saving data
    }
}


  render() {

const { navigate } = this.props.navigation;
   return(
    
        
      <View style={{flex: 1, paddingTop:25}}>
          <View style = {{padding: 5, backgroundColor: '#fff', alignItems: 'center', justifyContent: 'center'}}><Text style={{fontSize: 30, color: 'rgb(30,75,113)'}}>7Cs Gold</Text></View>
          
            <View style={{margin: 10}}>
                 <View style = {{padding: 5, backgroundColor: '#fff', marginBottom: 1, alignItems: 'center', justifyContent: 'center'}}>
                    <Text style={{fontSize: 24, color: 'rgb(30,75,113)'}}>Account</Text>

                 </View>
                 
               { this.state.flag ? (<TouchableHighlight onPress = { () => navigate('UserPrefer') }>
                   <Text style={{padding: 10, textAlign: 'center', marginBottom: 1, backgroundColor: '#fff', color: 'rgb(30,75,113)', fontSize: 18}}>Preferences</Text>
                </TouchableHighlight>) : (<View></View>) }
                <TouchableHighlight onPress = {() => navigate('Reset') }>
                   <Text style={{padding: 10, textAlign: 'center', marginBottom: 1, backgroundColor: '#fff', color: 'rgb(30,75,113)', fontSize: 18}}>Forgot Password</Text>
                </TouchableHighlight>
                 <TouchableHighlight onPress = {() => {this.logout()}}>
                   <Text style={{padding: 10, textAlign: 'center', marginBottom: 1, backgroundColor: '#fff', color: 'rgb(30,75,113)', fontSize: 18}}>Logout</Text>
                </TouchableHighlight>
            
            </View>
                
                <View style={{margin: 10}}>
                 <View style = {{padding: 5, backgroundColor: '#fff', marginBottom: 1, alignItems: 'center', justifyContent: 'center'}}>
                    <Text style={{fontSize: 24, color: 'rgb(30,75,113)'}}>Support</Text>

                 </View>
                 
                 <TouchableHighlight onPress = { () => navigate('About')}>
                   <Text style={{padding: 10, textAlign: 'center', marginBottom: 1, backgroundColor: '#fff', color: 'rgb(30,75,113)', fontSize: 18}}>About</Text>
                </TouchableHighlight>
                
                <TouchableHighlight onPress = { () => navigate('Privacy')}>
                   <Text style={{padding: 10, textAlign: 'center', marginBottom: 1, backgroundColor: '#fff', color: 'rgb(30,75,113)', fontSize: 18}}>Privacy Policy</Text>
                </TouchableHighlight>
                 <TouchableHighlight onPress = { () => navigate('Contact')}>
                   <Text style={{padding: 10, textAlign: 'center', marginBottom: 1, backgroundColor: '#fff', color: 'rgb(30,75,113)', fontSize: 18}}>Contact Us</Text>
                </TouchableHighlight>
            
            </View>
                
        
        
       
      </View>
  
  ); }
}
