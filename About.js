import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView
  
} from 'react-native';
import Ionicons from 'react-native-vector-icons/FontAwesome';

export default class About extends Component {

constructor(props)
{
  console.log("called");
    super(props);
    
    
    
   
}


  render() {

const { navigate } = this.props.navigation;
   return(
    
        
      <View style={{flex: 1}}>
      <ScrollView>
        <View style={{ margin: 10, padding: 10, backgroundColor: '#fff' }}>
            
                <Text style={{ fontWeight: 'bold' }}>7Cs Gold Business</Text>

                <Text style={{marginVertical: 10, textAlign: 'justify'}}>
                    7Cs gold and jewellery has a long legacy of delivering unique designs and high-quality products year after year. Right from its inception in 2009, our gold jewellery offering has flourished exponentially to extend its reach across the Middle East, North America, Australia and the South-east Asia. 
                </Text>
                <Text style={{marginVertical: 10, textAlign: 'justify'}}>
                    7CS Gold as a vision to become “The Desirable Destiny” for gold jewellery business by providing “One stop shop solution” to global retailers and wholesalers by leverage of modern technology.
                </Text>
                <Text style={{marginVertical: 10, textAlign: 'justify'}}>
                    In 2015, Our Initiative of creating a Web portal (www.7csgold.com) with 10,000+ designs and 12 regional collections (Kolkatta, Coimbatore, Rajkot, Turkey, Kuwait, Singapore, Hyderabad, Dubai..etc), which is a platform for us to invite and import global vendors and showcase their collections and designs along with our in-house collections 
                </Text>
                <Text style={{marginVertical: 10, textAlign: 'justify'}}>
                    In 2016, Another Initiative of creating Mobile Application “7csgold” to facilitate all our customers to place customized orders with Images, Audio and important specifications. This application has simplified order process flow and information flow between customers and us. It helped us to automate these orders by synchronize with our ERP system.
                </Text>
                <Text style={{marginVertical: 10, textAlign: 'justify'}}>
                    In 2017, Another Initiative of Offline Tab application “7CsB2B” is created with synchronizing all 3 business from 7Cs group company of Diamond Jewellery, Gold mounting and Plain Gold Jewellery business. This application will work offline even without internet and help our customers to showcase to his customer on handy.
                </Text>
                <Text style={{marginVertical: 10, textAlign: 'justify'}}>
                    In 2018, Launching this IOS/Android app connecting our global customers with all our reginal manufacturing units. Customers can set preferred reginal collection option and get frequent notification of new design launch. The app facilitate one touch order placement and track order status on the go…
                </Text>

            
        </View>
        </ScrollView>
      </View>
  
  ); }
}
