import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  
  
} from 'react-native';
import Ionicons from 'react-native-vector-icons/FontAwesome';

export default class Contact extends Component {

constructor(props)
{
 
    super(props);
    
    
    
   
}


  render() {

const { navigate } = this.props.navigation;
   return(
    
        
      <View style={{flex: 1}}>
        
        <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 15, paddingVertical: 10, marginBottom: 1, backgroundColor: '#fff'}}>
            <View>
                <Text>Facebook</Text>
                <Text>https://www.facebook.com/7CsGold/ </Text>
            </View>
            <Ionicons style={{alignSelf: 'center'}} name="facebook-square" size={24} color="rgb(30,75,113)" />
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 15, paddingVertical: 10, marginBottom: 1, backgroundColor: '#fff'}}>
            <View>
                <Text>Twitter</Text>
                <Text>https://twitter.com/7csb2b </Text>
            </View>
            <Ionicons style={{alignSelf: 'center'}} name="twitter-square" size={24} color="rgb(30,75,113)" />
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 15, paddingVertical: 10, marginBottom: 1, backgroundColor: '#fff'}}>
            <View>
                <Text>Instagram</Text>
                <Text>https://www.instagram.com/7csgold/ </Text>
            </View>
            <Ionicons style={{alignSelf: 'center'}} name="instagram" size={24} color="rgb(30,75,113)" />
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 15, paddingVertical: 10, marginBottom: 1, backgroundColor: '#fff'}}>
            <View>
                <Text>Pinterest</Text>
                <Text>https://www.pinterest.com/7csb2b/ </Text>
            </View>
            <Ionicons style={{alignSelf: 'center'}} name="pinterest-square" size={24} color="rgb(30,75,113)" />
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 15, paddingVertical: 10, marginBottom: 1, backgroundColor: '#fff'}}>
            <View>
                <Text>Website</Text>
                <Text>www.7csgold.com</Text><Text>www.7csb2b.com </Text>
            </View>
            <Ionicons style={{alignSelf: 'center'}} name="globe" size={24} color="rgb(30,75,113)" />
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 15, paddingVertical: 10, marginBottom: 1, backgroundColor: '#fff'}}>
            <View>
                <Text>Email</Text>
                <Text>7csweb@7cs.com </Text>
            </View>
            <Ionicons style={{alignSelf: 'center'}} name="envelope" size={24} color="rgb(30,75,113)" />
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 15, paddingVertical: 10, marginBottom: 1, backgroundColor: '#fff'}}>
            <View>
                <Text>Phone</Text>
                <Text>+971-4-2342064 </Text>
            </View>
            <Ionicons style={{alignSelf: 'center'}} name="phone" size={24} color="rgb(30,75,113)" />
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 15, paddingVertical: 10, marginBottom: 1, backgroundColor: '#fff'}}>
            <View>
                <Text>Sales Officee</Text>
                <Text>7Cs Gold & Jewellery LLC,</Text>
                <Text>Office No: 17, 1st Floor, Zone-4,</Text>
                <Text>Gold Center, Dubai, UAE</Text>
            </View>
            <Ionicons style={{alignSelf: 'center'}} name="location-arrow" size={24} color="rgb(30,75,113)" />
        </View>
        
       
      </View>
  
  ); }
}
