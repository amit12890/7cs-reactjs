import React, { Component } from "react"
import {
  View,
  Text,
  StyleSheet,
  Button,
  Image,
  ListView,
  AsyncStorage,
  ActivityIndicator,
  RefreshControl,
  TouchableHighlight,
  ScrollView
} from "react-native"

import LikeButton from "./LikeButton"
import ModalPanel from "./ModalPanel"
import Ionicons from "react-native-vector-icons/FontAwesome"
import Maticons from "react-native-vector-icons/Entypo"
import Dimensions from "Dimensions"
import Icon from "react-native-vector-icons/FontAwesome"
import { NavigationActions } from "react-navigation"
const { width, height } = Dimensions.get("window")
//import Upload from './Upload';

export default class MoreScreen extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { navigate } = this.props.navigation

    return (
      <ScrollView>
        <TouchableHighlight
          style={styles.catBtn}
          onPress={() =>
            navigate("List", {
              cat: "Dubai",
              filter: this.state.filter
            })
          }
        >
          <Text style={styles.categories}>Dubai</Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={styles.catBtn}
          onPress={() =>
            navigate("List", {
              cat: "Kolkata",
              filter: this.state.filter
            })
          }
        >
          <Text style={styles.categories}>Kolkata</Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={styles.catBtn}
          onPress={() =>
            navigate("List", {
              cat: "Finja",
              filter: this.state.filter
            })
          }
        >
          <Text style={styles.categories}>Finja</Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={styles.catBtn}
          onPress={() =>
            navigate("List", {
              cat: "Turkey",
              filter: this.state.filter
            })
          }
        >
          <Text style={styles.categories}>Turkey</Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={styles.catBtn}
          onPress={() =>
            navigate("List", {
              cat: "Kuwait",
              filter: this.state.filter
            })
          }
        >
          <Text style={styles.categories}>Kuwait</Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={styles.catBtn}
          onPress={() =>
            navigate("List", {
              cat: "Singapore",
              filter: this.state.filter
            })
          }
        >
          <Text style={styles.categories}>Singapore</Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={styles.catBtn}
          onPress={() =>
            navigate("List", {
              cat: "Mumbai",
              filter: this.state.filter
            })
          }
        >
          <Text style={styles.categories}>Mumbai</Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={styles.catBtn}
          onPress={() =>
            navigate("List", {
              cat: "Rajkot",
              filter: this.state.filter
            })
          }
        >
          <Text style={styles.categories}>Rajkot</Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={styles.catBtn}
          onPress={() =>
            navigate("List", {
              cat: "Delhi",
              filter: this.state.filter
            })
          }
        >
          <Text style={styles.categories}>Delhi</Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={styles.catBtn}
          onPress={() =>
            navigate("List", {
              cat: "Coimbatore",
              filter: this.state.filter
            })
          }
        >
          <Text style={styles.categories}>Coimbatore</Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={styles.catBtn}
          onPress={() =>
            navigate("List", {
              cat: "Hyderabad",
              filter: this.state.filter
            })
          }
        >
          <Text style={styles.categories}>Hyderabad</Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={styles.catBtn}
          onPress={() =>
            navigate("List", {
              cat: "Ahmedabad",
              filter: this.state.filter
            })
          }
        >
          <Text style={styles.categories}>Ahmedabad</Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={styles.catBtn}
          onPress={() =>
            navigate("List", {
              cat: "Jaipur",
              filter: this.state.filter
            })
          }
        >
          <Text style={styles.categories}>Jaipur</Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={styles.catBtn}
          onPress={() =>
            navigate("List", {
              cat: "Nellore",
              filter: this.state.filter
            })
          }
        >
          <Text style={styles.categories}>Nellore</Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={styles.catBtn}
          onPress={() =>
            navigate("List", {
              cat: "Sri Lanka",
              filter: this.state.filter
            })
          }
        >
          <Text style={styles.categories}>Sri Lanka</Text>
        </TouchableHighlight>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "#F5FCFF",
    padding: 10,
    paddingTop: 80
  },
  input: {
    height: 50,
    width: 300,
    marginTop: 10,
    padding: 4,
    fontSize: 18,
    borderWidth: 1,
    borderColor: "#48bbec"
  },
  catBtn: {
    height: 50,
    backgroundColor: "rgb(30,75,113)",
    alignSelf: "stretch",
    marginTop: 10,
    padding: 10,
    justifyContent: "center",
    borderRadius: 50
  },
  categories: {
    color: "#fff"
  },
  button: {
    height: 50,
    backgroundColor: "#48BBEC",
    alignSelf: "stretch",
    marginTop: 10,
    justifyContent: "center"
  },
  buttonText: {
    fontSize: 22,
    color: "#FFF",
    alignSelf: "center"
  },
  heading: {
    fontSize: 30
  },
  error: {
    color: "red",
    paddingTop: 10
  },
  loader: {
    marginTop: 20
  },
  stretch: {
    width: 300,
    height: 300,
    alignSelf: "center"
  }
})
