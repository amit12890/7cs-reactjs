import React, { Component } from "react"
import { View, Text, StyleSheet, Button, Image, ListView, AsyncStorage, TextInput, ActivityIndicator, TouchableHighlight, RefreshControl, ScrollView, Keyboard, Alert, Picker, Modal, Platform } from "react-native"

import AudioRecord from "./AudioRecord"
import Icon from "react-native-vector-icons/FontAwesome"
import FAB from "react-native-fab"
import GridView from "react-native-easy-gridview"
import { NavigationActions } from "react-navigation"
import Dimensions from "Dimensions"
const {width, height} = Dimensions.get("window")
import PushNotifications from "./push_notifications"
import { Checkbox, CheckboxGroup } from "react-native-material-design"
import HudView from "react-native-easy-hud"
import LikeButton from "./LikeButton"
import ModalPanel from "./ModalPanel"
import Imagezoom from "react-native-transformable-image"
import md5 from "react-native-md5"
import ModalDropdown from "react-native-modal-dropdown"

const ACCESS_TOKEN = "access_token"
const USER_ID = "user_id"
let _this = null
export default class ListScreen extends Component {
  static navigationOptions = ({navigation}) => ({
    title: navigation.state.params.cat,
    headerRight: (
    <TouchableHighlight
    style={{
      marginRight: 20
    }}
    onPress={() => _this.toggleFilterModal()}
    >
        <Icon
    name="filter"
    style={{
      fontSize: 20
    }}
    />
      </TouchableHighlight>
    )
  })

  constructor(props) {
    super(props)

    var ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
    var tds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
    this.state = {
      orderPlaced: false,
      selected: [],
      bulkModalVisible: false,
      refreshing: false,
      json: [],
      tempJson: [],
      checked: [],
      collection: [],
      karatage: "",
      qty: "",
      uid: "",
      email: "",
      modaluri: "",
      audio: null,
      accessToken: "",
      bottom: 0,
      type: "",
      remarks: "",
      modalVisible: false,
      filterModal: false,
      result: "",
      templist: tds.cloneWithRows([]),
      clonelist: ds.cloneWithRows([]),
      karatage: "22",
      page_no: 0
    }
  }

  async componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      this._keyboardDidShow.bind(this)
    )
    this.keyboardDidHideListener = Keyboard.addListener(
      "keyboardDidHide",
      this._keyboardDidHide.bind(this)
    )

    const {params} = this.props.navigation.state
    let cat = params.cat
    let url = "http://b2bapp.7csgold.com/api/get_collection.php?id=" + cat
    console.log(url)
    await fetch(url, {
      method: "GET"
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log(responseJson)

        let newColl = []
        let collection = responseJson.collection
        collection.forEach(function(value) {
          console.log(value)
          value.label = "\n" + value.label
          newColl.push(value)
        })

        this.setState({
          collection: newColl
        })
      })
      .catch(error => {
        console.error(error)
      })

  // await PushNotifications()
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove()
    this.keyboardDidHideListener.remove()
  }

  _keyboardDidShow() {
    // console.log(this.state.bottom);
    this.setState({
      bottom: 130
    })
  }

  _keyboardDidHide() {
    this.setState({
      bottom: 0
    })
  }

  toggleFilterModal(flag = true) {
    this.setState({
      filterModal: flag
    })
    if (flag == false) {
      this._onRefresh()
    }
  }

  toggleModal(visible) {
    if (visible) {
      for (var i = 0; i < this.state.json.length; i++) {
        for (var j = 0; j < this.state.selected.length; j++) {
          if (this.state.json[i].id == this.state.selected[j]) {
            this.state.tempJson.push(this.state.json[i])
          }
        }
      }
      var tds = new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2
      })
      this.setState({
        cartItems: this.state.tempJson,
        templist: tds.cloneWithRows(this.state.tempJson)
      })
      this.state.tempJson = []
    } else {
      var tds = new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2
      })
      var ds = new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2
      })

      if (this.state.selected.length == 1) {
        this.state.selected = []

        for (var i = 0; i < this.state.json.length; i++) {
          this.state.json[i].checked = false
        }
      }

      this.setState({
        templist: tds.cloneWithRows([]),
        clonelist: ds.cloneWithRows(this.state.json)
      })
    }

    this.setState({
      bulkModalVisible: visible
    })
  }

  setModalVisible(visible, img = "") {
    this.setState({
      modalVisible: visible
    })
    this.setState({
      modaluri: img
    })
  }

  loadData() {
    this.setState({
      refreshing: true
    })
    let user_id = this.state.uid
    const {params} = this.props.navigation.state
    let cat = params.cat
    let selectedFilter = this.state.checked
    let filter = params.filter
    let page_no = this.state.page_no

    let url = "http://b2bapp.7csgold.com/api/customer_feed.php?id=" +
      user_id +
      "&cat=" +
      cat +
      "&filter=" +
      filter +
      "&page_no=" +
      page_no +
      "&page_size=" +
      5

    if (selectedFilter != null && selectedFilter.length > 0) {
      url = url + "&sub_category=" + selectedFilter
    }

    console.log(url)
    fetch(url, {
      method: "GET"
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log(responseJson)
        var products = responseJson.product
        if (page_no != 0) {
          var oldProducts = this.state.json
          for (let i = 0; i < products.length; i++) {
            let product = products[i]
            oldProducts.push(product)
          }
          products = oldProducts
        }

        this.setState({
          json: products
        })

        var ds = new ListView.DataSource({
          rowHasChanged: (r1, r2) => r1 !== r2
        })
        this.setState({
          clonelist: ds.cloneWithRows(products)
        })
      })

      .catch(error => {
        console.error(error)
      })
    this.setState({
      refreshing: false
    })
  }

  _onRefresh() {
    let selectedValues = []
    this.setState(
      {
        page_no: 0,
        selected: selectedValues
      },
      () => {
        this.loadData()
      }
    )
  }
  async setSession() {
    try {
      const value = await AsyncStorage.getItem(ACCESS_TOKEN)
      const user_id = await AsyncStorage.getItem(USER_ID)
      if (value !== null) {
        // We have data!!
        console.log("List Set Session")
        this.setState({
          accessToken: value
        })
        this.setState({
          uid: user_id
        })
      }
    } catch (error) {
      console.log(error)
    // Error retrieving data
    }
  }
  async storeToken(responseData) {
    console.log(responseData)
    try {
      await AsyncStorage.setItem(ACCESS_TOKEN, responseData)
    } catch (error) {
      console.log("storage error")
    // Error saving data
    }
  }

  addProduct = value => {
    let selectedValues = this.state.selected
    if (selectedValues.indexOf(value) > -1) {
      //In the array!
    } else {
      //Not in the array
      selectedValues = [...selectedValues, value]
    }

    var listItems = this.state.json
    var cartItemFiltered = listItems.filter(task => task.id == value)
    var item = cartItemFiltered[0]
    item.checked = true
    index = listItems.indexOf(item)
    listItems[index] = item

    var ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
    this.setState({
      json: listItems,
      clonelist: ds.cloneWithRows(listItems),
      selected: selectedValues
    }, () => {
      this.toggleModal(true)
    })
  }

  deleteCartItem = rowId => {
    var cartItems = this.state.cartItems
    var cartItemFiltered = cartItems.filter(task => task.id == rowId)
    var index = cartItems.indexOf(cartItemFiltered[0])

    cartItems.splice(index, 1)

    var tds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })

    //Remove selection fro FAB
    let selectedValues = this.state.selected
    index = selectedValues.indexOf(rowId)

    if (index == -1) {
      //Add
    } else {
      //Do-nothing
      selectedValues = [
        ...selectedValues.slice(0, index),
        ...selectedValues.slice(index + 1)
      ]
    }

    var listItems = this.state.json
    var cartItemFiltered = listItems.filter(task => task.id == rowId)
    var item = cartItemFiltered[0]
    item.checked = false
    index = listItems.indexOf(item)
    listItems[index] = item

    var ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
    this.setState({
      selected: selectedValues,
      cartItems: cartItems,
      templist: tds.cloneWithRows(cartItems),
      clonelist: ds.cloneWithRows(listItems)
    })
    this.state.tempJson = []
    if (cartItems.length > 0) {
    } else {
      console.log(this.state.tempJson)
      var ds = new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2
      })
      this.setState({
        clonelist: ds.cloneWithRows(this.state.json),
        selected: []
      })
      this.state.tempJson = []

      this.toggleModal(false)
    }
  }

  _onChange = (checked, value) => {
    let selectedValues = this.state.selected
    let index = selectedValues.indexOf(value)

    if (index == -1) {
      //Add
      selectedValues = [...selectedValues, value]
    } else {
      //Do-nothing
      selectedValues = [
        ...selectedValues.slice(0, index),
        ...selectedValues.slice(index + 1)
      ]
    }

    var listItems = this.state.json
    var cartItemFiltered = listItems.filter(task => task.id == value)
    var item = cartItemFiltered[0]
    item.checked = checked
    index = listItems.indexOf(item)
    listItems[index] = item

    var ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
    this.setState({
      json: listItems,
      clonelist: ds.cloneWithRows(listItems),
      selected: selectedValues
    })
  }

  async setFilter(values) {
    console.log(values)
    this.setState({
      checked: values
    })

  // if (values && values.length > 0) {
  //   for (var i = this.state.json.length - 1; i >= 0; i--) {
  //     for (var j = values.length - 1; j >= 0; j--) {
  //       if (values[j] === this.state.json[i].sub_category) {
  //         this.state.tempJson.push(this.state.json[i])
  //         break
  //       } else {
  //         continue
  //       }
  //     }
  //   }
  //   console.log(this.state.tempJson)
  //   var ds = new ListView.DataSource({
  //     rowHasChanged: (r1, r2) => r1 !== r2
  //   })
  //   this.setState({
  //     clonelist: ds.cloneWithRows(this.state.tempJson)
  //   })
  //   this.state.tempJson = []
  // } else {
  //   console.log(this.state.tempJson)
  //   var ds = new ListView.DataSource({
  //     rowHasChanged: (r1, r2) => r1 !== r2
  //   })
  //   this.setState({
  //     clonelist: ds.cloneWithRows(this.state.json)
  //   })
  //   this.state.tempJson = []
  // }
  }

  async getToken() {
    try {
      const value = await AsyncStorage.getItem(ACCESS_TOKEN)
      if (value !== null) {
        // We have data!!
        console.log("has storage")
        console.log(value)
      }
    } catch (error) {
      console.log(error)
    // Error retrieving data
    }
  }

  async selectAll() {
    console.log(!this.state.selectAll)
    let flag = !this.state.selectAll

    this.setState({
      selectAll: flag
    })

    if (flag) {
      let array = []

      for (var i = 0; i < this.state.collection.length; i++) {
        array[i] = this.state.collection[i].value
      }

      this.refs.cbg1.value = array
      this.setState({
        backgroundColor: "rgb(30,75,113)"
      })
      this.setState({
        color: "#fff"
      })
      await this.setState({
        checked: array
      })
    } else {
      let array = []
      this.refs.cbg1.value = array
      this.setState({
        backgroundColor: "#fff"
      })
      this.setState({
        color: "#000"
      })
      await this.setState({
        checked: array
      })
    }
  }

  async componentDidMount() {
    // console.log("hello");
    _this = this
    await this.setSession()
    let user_id = this.state.uid
    const {params} = this.props.navigation.state
    let cat = params.cat
    let filter = params.filter
    let url = "http://b2bapp.7csgold.com/api/customer_feed.php?id=" +
      user_id +
      "&cat=" +
      cat +
      "&filter=" +
      filter +
      "&page_no=" +
      this.state.page_no +
      "&page_size=" +
      5

    console.log(url)
    await fetch(url, {
      method: "GET"
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log(responseJson)
        this.setState({
          json: responseJson.product
        })
        var ds = new ListView.DataSource({
          rowHasChanged: (r1, r2) => r1 !== r2
        })
        this.setState({
          clonelist: ds.cloneWithRows(responseJson.product)
        })
      })
      .catch(error => {
        console.error(error)
      })
  }

  async placeBulkOrder() {
    if (this.state.selected.length > 1) {
      if (this.validateValues() == true) {
        console.log("bulk order clicked")
        this.setState({
          orderPlaced: true
        })

        try {
          let rand = md5.b64_md5(Date.now() + this.state.uid)

          let audio = rand + (Platform.OS === "ios" ? ".caf" : ".m4a")
          var product_ids = this.state.selected.join()

          this.setState({
            loading: true
          })
          var data = new FormData()
          if (this.state.audio != null) {
            data.append("audio", {
              uri: this.state.audio,
              name: audio,
              type: "audio/m4a"
            })
          }
          data.append("user_id", this.state.uid)
          data.append("product_id", product_ids)
          data.append("karatage", this.state.karatage)
          data.append("qty", this.state.qty)
          data.append("remarks", this.state.remarks)
          console.log(data)
          const config = {
            method: "POST",
            headers: {
              Accept: "application/json",
              "Content-Type": "multipart/form-data"
            },
            body: data
          }

          let response = await fetch(
            "http://b2bapp.7csgold.com/api/customer_bulk_order.php",
            config
          )
          let responseJson = await response.json()
          console.log(responseJson)
          if (response.status >= 200 && response.status < 300) {
            //Handle success
            let status = responseJson.status
            if (status) {
              Alert.alert(
                "Order Placed",
                "Thankyou for placing the order.",
                [
                  {
                    text: "OK",
                    onPress: () => {
                      this.toggleModal(false)
                      this.props.navigation.dispatch(NavigationActions.back())
                      setTimeout(
                        this.props.navigation.navigate.bind(null, "Order"),
                        1000
                      )
                    }
                  }
                ],
                {
                  cancelable: false
                }
              )

              this.setState({
                orderPlaced: false
              })
            } else {
              Alert.alert("Problem Occured", "Please Try Again")
            }
          //this.redirect('home');
          } else {
            //Handle error
            let error = res
            throw error
            Alert.alert("Problem Occured", "Please Try Again")
          }
        } catch (error) {
          console.error(error)
        }
      }
    } else {
      if (this.validateValues() == true) {
        console.log("bulk order clicked")
        this.setState({
          orderPlaced: true
        })

        try {
          let rand = md5.b64_md5(Date.now() + this.state.uid)
          let audio = rand + (Platform.OS === "ios" ? ".caf" : ".m4a")

          this.setState({
            loading: true
          })

          var data = new FormData()
          if (this.state.audio != null) {
            data.append("audio", {
              uri: this.state.audio,
              name: audio,
              type: "audio/m4a"
            })
          }
          data.append("user_id", this.state.uid)
          data.append("product_id", this.state.selected[0])
          data.append("karatage", this.state.karatage)
          data.append("qty", this.state.qty)
          data.append("remarks", this.state.remarks)
          console.log(data)
          const config = {
            method: "POST",
            headers: {
              Accept: "application/json",
              "Content-Type": "multipart/form-data"
            },
            body: data
          }

          let response = await fetch(
            "http://b2bapp.7csgold.com/api/customer_post_order.php",
            config
          )
          let responseJson = await response.json()
          if (response.status >= 200 && response.status < 300) {
            //Handle success
            let status = responseJson.status
            if (status) {
              Alert.alert(
                "Order Placed",
                "Thankyou for placing the order.",
                [
                  {
                    text: "OK",
                    onPress: () => {
                      this.toggleModal(false)
                      this.props.navigation.dispatch(NavigationActions.back())
                      setTimeout(
                        this.props.navigation.navigate.bind(null, "Order"),
                        1000
                      )
                    }
                  }
                ],
                {
                  cancelable: false
                }
              )

              this.setState({
                orderPlaced: false
              })
            } else {
              Alert.alert("Problem Occured", "Please Try Again")
            }
          //this.redirect('home');
          } else {
            //Handle error
            let error = res
            throw error
            Alert.alert("Problem Occured", "Please Try Again")
          }
        } catch (error) {
          console.error(error)
        }
      }
    }
  }

  setAudioUrl = data => {
    console.log(data)
    this.setState({
      audio: data
    })
  }

  _dropdownkaratage(idx, value) {
    this.setState({
      karatage: value
    })
  }

  validateValues() {
    var errmsg = ""

    if (this.state.karatage.length <= 0) {
      errmsg = "Enter karatage"
    } else if (this.state.qty.length <= 0) {
      errmsg = "Enter quantity"
    }

    if (errmsg.length > 0) {
      Alert.alert(errmsg)
      return false
    } else {
      return true
    }
  }

  loadMore() {
    if (this.state.refreshing == false) {
      this.setState({
        page_no: this.state.page_no + 1
      })
      this.loadData()
    }
  }

  _hud: HudView

  render() {
    return (
      <View
      style={{
        flex: 1
      }}
      >
        <Modal
      animationType={"slide"}
      transparent={false}
      visible={this.state.filterModal}
      onRequestClose={() => {
        this.toggleFilterModal(false)
      }}
      style={{
        flexDirection: "row",
        margin: 20
      }}
      >
          <View
      style={{
        flexDirection: "row",
        backgroundColor: "#fff"
      }}
      >
            <Icon
      name="chevron-left"
      size={25}
      color="rgb(30,75,113)"
      style={{
        padding: 5,
        margin: 5
      }}
      onPress={() => this.toggleFilterModal(false)}
      />
            <Text
      style={{
        fontSize: 30,
        color: "rgb(30,75,113)",
        textAlign: "center",
        flex: 1,
        marginTop: 5
      }}
      >
              7Cs Gold
            </Text>
          </View>
          <View
      style={{
        flex: 0.8,
        marginTop: 30,
        marginLeft: 30,
        marginRight: 30
      }}
      >
            <TouchableHighlight
      style={{
        height: 50,
        borderWidth: 1,
        borderColor: "rgb(30,75,113)",
        backgroundColor: this.state.backgroundColor,
        borderRadius: 50,
        alignSelf: "stretch",
        marginBottom: 10,
        justifyContent: "center"
      }}
      onPress={() => this.selectAll()}
      >
              <Text
      style={{
        fontSize: 18,
        color: this.state.color,
        alignSelf: "center"
      }}
      >
                Select All
              </Text>
            </TouchableHighlight>
            <ScrollView>
              <CheckboxGroup
      ref="cbg1"
      onSelect={values => {
        this.setFilter(values)
      }}
      theme={"light"}
      checked={this.state.checked}
      items={this.state.collection}
      />
            </ScrollView>
            <TouchableHighlight
      style={styles.mbutton}
      onPress={() => this.toggleFilterModal(false)}
      >
              <Text style={styles.mbuttonText}>Set Filter</Text>
            </TouchableHighlight>
          </View>
        </Modal>
        <Modal
      animationType={"slide"}
      transparent={false}
      visible={this.state.bulkModalVisible}
      onRequestClose={() => {
        this.toggleModal(false)
      }}
      >
          <View
      style={{
        padding: 20,
        bottom: this.state.bottom
      }}
      >
            <View
      style={{
        flexDirection: "row",
        padding: 5,
        backgroundColor: "#fff"
      }}
      >
              <Icon
      name="chevron-left"
      size={25}
      color="rgb(30,75,113)"
      style={{
        padding: 5,
        margin: 5
      }}
      onPress={() => this.toggleModal(false)}
      />
              <Text
      style={{
        fontSize: 30,
        color: "rgb(30,75,113)",
        textAlign: "center",
        flex: 1,
        marginTop: 5
      }}
      >
                7Cs Gold
              </Text>
            </View>
            <ScrollView
      style={{
        marginBottom: 10
      }}
      >
              <AudioRecord url={this.setAudioUrl} />
              <View
      style={{
        flexDirection: "row",
        flex: 1
      }}
      >
                <Text
      style={{
        fontSize: 16,
        color: "rgb(30,75,113)",
        flex: 0.4,
        marginTop: 40,
        paddingLeft: 10
      }}
      >
                  Karatage
                </Text>
                <Text
      style={{
        fontSize: 16,
        color: "rgb(30,75,113)",
        flex: 0.2,
        marginTop: 40
      }}
      >
                  :
                </Text>
                <ModalDropdown
      style={{
        fontSize: 16,
        color: "rgb(30,75,113)",
        marginTop: 40,
        width: 100
      }}
      textStyle={{
        fontSize: 16,
        color: "rgb(30,75,113)"
      }}
      dropdownStyle={{
        fontSize: 16,
        color: "rgb(30,75,113)"
      }}
      dropdownTextStyle={{
        fontSize: 16,
        color: "rgb(30,75,113)"
      }}
      dropdownTextHighlightStyle={{
        fontSize: 16,
        color: "rgb(30,75,113)"
      }}
      defaultValue="22"
      defaultIndex="0"
      options={["22", "21", "18"]}
      onSelect={(idx, value) => this._dropdownkaratage(idx, value)}
      />
              </View>

              <View
      style={{
        flexDirection: "row",
        alignSelf: "stretch"
      }}
      >
                <TextInput
      style={{
        fontSize: 16,
        color: "rgb(30,75,113)",
        padding: 10,
        width: 300
      }}
      onChangeText={text => this.setState({
        qty: text
      })
      }
      placeholder="Size/Weight/Qty"
      />
              </View>
              <TextInput
      style={{
        fontSize: 16,
        color: "rgb(30,75,113)",
        height: 70,
        padding: 10
      }}
      multiline={true}
      numberOfLines={4}
      onChangeText={text => this.setState({
        remarks: text
      })
      }
      value={this.state.remarks}
      placeholder="Remarks"
      />

              <GridView
      dataSource={this.state.templist}
      renderRow={rowData => (
        <View
        style={{
          marginHorizontal: 10,
          marginTop: 20,
          padding: 10,
          paddingTop: 20,
          backgroundColor: "#fff"
        }}
        >
                    <Icon
        name="times"
        size={25}
        color="red"
        style={{
          padding: 5,
          margin: 5
        }}
        onPress={() => this.deleteCartItem(rowData.id)}
        />
                    <Image
        style={{
          width: 100,
          height: 100
        }}
        source={{
          uri: rowData.img_url
        }}
        />
                  </View>
      )}
      numberOfItemsPerRow={2}
      removeClippedSubviews={false}
      initialListSize={1}
      pageSize={5}
      />
              {this.state.orderPlaced ? (
        <ActivityIndicator
        animating={this.state.orderPlaced}
        color="rgb(30,75,113)"
        size="large"
        style={styles.activityIndicator}
        />
        ) : (
        <TouchableHighlight
        style={styles.mbutton}
        onPress={() => this.placeBulkOrder()}
        >
                  <Text style={styles.mbuttonText}>Order</Text>
                </TouchableHighlight>
        )}
            </ScrollView>
          </View>
        </Modal>
        <Modal
      animationType="slide"
      transparent={false}
      visible={this.state.modalVisible}
      onRequestClose={() => {
        this.setModalVisible(!this.state.modalVisible)
      }}
      >
          <View
      style={{
        flex: 1,
        backgroundColor: "#000",
        justifyContent: "center"
      }}
      >
            <View
      style={{
        flexDirection: "column"
      }}
      >
              <TouchableHighlight
      onPress={() => this.setModalVisible(!this.state.modalVisible)}
      style={{
        marginTop: 10
      }}
      >
                <Text
      style={{
        fontSize: 20,
        marginTop: 40,
        marginLeft: 20,
        zIndex: 1,
        color: "#fff"
      }}
      onPress={() => this.setModalVisible(!this.state.modalVisible)}
      >
                  X
                </Text>
              </TouchableHighlight>
              <Imagezoom
      style={styles.modalimg}
      source={{
        uri: this.state.modaluri
      }}
      />
            </View>
          </View>
        </Modal>

        <ListView
      refreshControl={
      <RefreshControl
      refreshing={this.state.refreshing}
      onRefresh={this._onRefresh.bind(this)}
      />
      }
      onEndReached={() => this.loadMore()}
      dataSource={this.state.clonelist}
      renderRow={rowData => (
        <View
        style={{
          marginHorizontal: 10,
          marginTop: 20,
          padding: 10,
          paddingTop: 20,
          backgroundColor: "#fff"
        }}
        >
              <Checkbox
        value={rowData.id}
        onCheck={this._onChange}
        checked={rowData.checked}
        />
              <TouchableHighlight
        underlayColor={"#fff"}
        onPress={() => this.setModalVisible(true, rowData.img_url)}
        >
                <Image
        style={styles.stretch}
        source={{
          uri: rowData.img_url
        }}
        />
              </TouchableHighlight>

              <View
        style={{
          flexDirection: "row",
          marginHorizontal: 15,
          marginTop: 10,
          alignSelf: "center",
          justifyContent: "space-between"
        }}
        >
                <Text
        style={{
          flex: 1,
          fontSize: 18,
          color: "rgb(30,75,113)"
        }}
        >
                  {rowData.karatage} kt{" "}
                </Text>
                <Text
        style={{
          flex: 1,
          fontSize: 18,
          color: "rgb(30,75,113)"
        }}
        >
                  {rowData.weight} gm{" "}
                </Text>
                <LikeButton
        style={{
          flex: 1,
          flexDirection: "row"
        }}
        id={rowData.id}
        liked={rowData.like}
        />
              </View>

              <Text
        style={{
          alignSelf: "stretch",
          marginHorizontal: 20,
          color: "rgb(30,75,113)"
        }}
        >
                Remarks: {rowData.remarks}{" "}
              </Text>
              <View
        style={{
          flexDirection: "row",
          alignSelf: "center"
        }}
        >
                <TouchableHighlight
        style={styles.orderButton}
        onPress={() => {
          this.addProduct(rowData.id)
        }}
        >
                  <Text
        style={{
          color: "#fff",
          textAlign: "center"
        }}
        >
                    Order
                  </Text>
                </TouchableHighlight>
              </View>
            </View>
      )}
      enableEmptySections={true}
      />
        <FAB
      buttonColor="rgb(30,75,113)"
      iconTextColor="#FFFFFF"
      onClickAction={() => {
        this.toggleModal(true)
      }}
      visible={this.state.selected.length > 0}
      iconTextComponent={<Text> {this.state.selected.length} </Text>}
      />

        <HudView
      ref={hud => {
        this._hud = hud
      }}
      delay={1.5}
      />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "#F5FCFF",
    padding: 10,
    paddingTop: 80
  },
  input: {
    height: 50,
    width: 300,
    marginTop: 10,
    padding: 4,
    fontSize: 18,
    borderWidth: 1,
    borderColor: "#48bbec"
  },
  button: {
    height: 50,
    backgroundColor: "#48BBEC",
    alignSelf: "stretch",
    marginTop: 30,
    justifyContent: "center",
    position: "absolute"
  },
  mbutton: {
    height: 50,
    backgroundColor: "rgb(30,75,113)",
    borderRadius: 50,

    alignSelf: "stretch",
    marginTop: 10,
    marginBottom: 30,
    justifyContent: "center"
  },
  mbuttonText: {
    fontSize: 18,
    color: "#FFF",
    alignSelf: "center"
  },
  buttonText: {
    fontSize: 22,
    color: "#FFF",
    alignSelf: "center"
  },
  heading: {
    fontSize: 30
  },
  error: {
    color: "red",
    paddingTop: 10
  },
  loader: {
    marginTop: 20
  },
  stretch: {
    width: 300,
    height: 300,
    alignSelf: "center"
  },
  modalimg: {
    width: width,
    height: height - 50,
    alignSelf: "center",
    justifyContent: "center"
  },
  orderButton: {
    color: "#fff",
    width: 150,
    margin: 10,
    padding: 10,
    fontSize: 18,
    textAlign: "center",
    backgroundColor: "rgb(30,75,113)"
  }
})
