import React, { Component } from "react"
import {
  View,
  Text,
  StyleSheet,
  Button,
  Image,
  ListView,
  ScrollView,
  AsyncStorage,
  ActivityIndicator,
  TouchableHighlight,
  Alert
} from "react-native"
import HudView from "react-native-easy-hud"
import { Checkbox, CheckboxGroup } from "react-native-material-design"
//import Upload from './Upload';

const ACCESS_TOKEN = "access_token"
const USER_ID = "user_id"
const TYPE = "type"
export default class UserPreferScreen extends Component {
  constructor(props) {
    super(props)

    var ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
    this.state = {
      refreshing: false,
      uid: "",
      email: "",
      modaluri: "",
      accessToken: "",
      type: "",
      modalVisible: false,
      result: "",
      status: false,
      loading: true,
      backgroundColor: "#fff",
      color: "#000",
      selectAll: false,
      checked: [],
      category: [],
      clonelist: ds.cloneWithRows([]),
      allCategories: []
    }

    this.precheck()
  }

  async precheck() {
    // console.log("hello");

    await this.setSession()
    let user_id = this.state.uid

    let url = "http://b2bapp.7csgold.com/api/get_category.php?id=" + user_id

    await fetch(url, {
      method: "GET"
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log(responseJson)

        let newColl = []
        let collection = responseJson.all
        collection.forEach(function(value) {
          console.log(value)
          let newVal = []
          newVal.value = value
          newVal.label = "\n" + value
          newColl.push(newVal)
        })

        this.setState({
          checked: responseJson.checked,
          allCategories: newColl,
          status: true
        })
      })
      .catch(error => {
        console.error(error)
      })

    this.setState({
      loading: false
    })
  }

  async setUserPrefer() {
    //this.refs.cbg1.value = ['Dubai'];
    this._hud.show()
    try {
      let uid = await AsyncStorage.getItem(USER_ID)

      // this.redireu

      let response = await fetch(
        "http://b2bapp.7csgold.com/api/put_category.php",
        {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            id: uid,
            category: this.state.category
          })
        }
      )
      let res = await response.json()
      console.log(res)
      if (response.status >= 200 && response.status < 300) {
        this._hud.hide()
        if (res.status) {
          // this.getToken();
          const { navigate } = this.props.navigation
          let type = await AsyncStorage.getItem(TYPE)
          if (type == "customer") {
            navigate("Home")
          } else {
            navigate("Employee")
          }

          Alert.alert(
            "Thank you",
            "Now you can get updates on selected Preferences"
          )
        } else {
          this._hud.hide()
          Alert.alert("Something went wrong", "Try again after sometime")
        }

        // this.redirect('root');
      } else {
        this._hud.hide()
        Alert.alert("Something went wrong", "Try again after sometime")
      }
    } catch (error) {
      //this.redirect('login');
      this._hud.hide()
      Alert.alert("Something went wrong", "Try again after sometime")
    }
  }

  async setSession() {
    try {
      const value = await AsyncStorage.getItem(ACCESS_TOKEN)
      const user_id = await AsyncStorage.getItem(USER_ID)
      if (value !== null) {
        // We have data!!
        console.log("Set Session")
        console.log(user_id)
        this.setState({
          accessToken: value
        })
        this.setState({
          uid: user_id
        })
      }
    } catch (error) {
      console.log(error)
      // Error retrieving data
    }
  }
  async storeToken(responseData) {
    console.log(responseData)
    try {
      await AsyncStorage.setItem(ACCESS_TOKEN, responseData)
    } catch (error) {
      console.log("storage error")
      // Error saving data
    }
  }

  async getToken() {
    try {
      const value = await AsyncStorage.getItem(ACCESS_TOKEN)
      if (value !== null) {
        // We have data!!
        console.log("has storage")
        console.log(value)
      }
    } catch (error) {
      console.log(error)
      // Error retrieving data
    }
  }

  async setCategory(values) {
    await this.setState({
      category: values
    })
    console.log(this.state.category)
  }

  async selectAll() {
    console.log(!this.state.selectAll)
    let flag = !this.state.selectAll

    this.setState({
      selectAll: flag
    })

    if (flag) {
      // let array = [
      //   "Dubai",
      //   "Kolkata",
      //   "Finja",
      //   "Turkey",
      //   "Kuwait",
      //   "Singapore",
      //   "Mumbai",
      //   "Rajkot",
      //   "Delhi",
      //   "Coimbatore",
      //   "Hyderabad",
      //   "Ahmedabad",
      //   "Jaipur",
      //   "Nellore",
      //   "Sri Lanka"
      // ]

      let array = []
      let collection = this.state.allCategories
      collection.forEach(function(value) {
        console.log(value)
        array.push(value.value)
      })

      this.refs.cbg1.value = array
      this.setState({
        backgroundColor: "rgb(30,75,113)"
      })
      this.setState({
        color: "#fff"
      })
      await this.setState({
        category: array
      })
    } else {
      let array = []
      this.refs.cbg1.value = array
      this.setState({
        backgroundColor: "#fff"
      })
      this.setState({
        color: "#000"
      })
      await this.setState({
        category: array
      })
    }
  }

  _hud: HudView

  render() {
    if (this.state.loading) {
      return (
        <View style={styles.activityContainer}>
          <ActivityIndicator
            animating={this.state.loading}
            color="rgb(30,75,113)"
            size="large"
            style={styles.activityIndicator}
          />
        </View>
      )
    } else {
      return (
        <View
          style={{
            flex: 1,
            margin: 20
          }}
        >
          <TouchableHighlight
            style={{
              height: 50,
              borderWidth: 1,
              borderColor: "rgb(30,75,113)",
              backgroundColor: this.state.backgroundColor,
              borderRadius: 50,
              alignSelf: "stretch",
              marginBottom: 10,
              justifyContent: "center"
            }}
            onPress={() => this.selectAll()}
          >
            <Text
              style={{
                fontSize: 18,
                color: this.state.color,
                alignSelf: "center"
              }}
            >
              Select All
            </Text>
          </TouchableHighlight>

          <ScrollView>
            {this.state.status ? (
              <CheckboxGroup
                ref="cbg1"
                onSelect={values => {
                  this.setCategory(values)
                }}
                checked={this.state.checked}
                items={this.state.allCategories}
              />
            ) : (
              <Text> Please Wait...</Text>
            )}
          </ScrollView>
          <TouchableHighlight
            style={styles.button}
            onPress={() => this.setUserPrefer()}
          >
            <Text style={styles.buttonText}>Set Preferrences</Text>
          </TouchableHighlight>
          <HudView
            ref={hud => {
              this._hud = hud
            }}
            delay={1.5}
          />
        </View>
      )
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'flex-start',
    alignItems: "center",
    backgroundColor: "rgb(248,248,248)"
  },
  activityContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 70
  },
  activityIndicator: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    height: 80
  },
  input: {
    height: 50,
    width: 300,
    marginTop: 10,
    paddingHorizontal: 10,
    paddingVertical: 10,
    backgroundColor: "#eee",
    borderBottomColor: "#eee",
    borderRadius: 10,
    bottom: 20,
    fontSize: 18
  },
  button: {
    height: 50,
    backgroundColor: "rgb(30,75,113)",
    borderRadius: 50,

    alignSelf: "stretch",
    marginTop: 10,
    marginBottom: 10,
    justifyContent: "center"
  },
  Sbutton: {
    height: 50,
    backgroundColor: "rgb(30,75,113)",
    borderRadius: 50,

    alignSelf: "stretch",

    marginBottom: 10,
    justifyContent: "center"
  },

  buttonText: {
    fontSize: 18,
    color: "#FFF",
    alignSelf: "center"
  },
  heading: {
    fontSize: 30
  },
  error: {
    color: "red",
    paddingTop: 10
  },
  loader: {
    marginTop: 20
  }
})
