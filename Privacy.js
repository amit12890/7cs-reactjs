import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  
  
} from 'react-native';
import Ionicons from 'react-native-vector-icons/FontAwesome';

export default class Privacy extends Component {

constructor(props)
{
 
    super(props);
    
    
    
   
}


  render() {

const { navigate } = this.props.navigation;
   return(
    
    <View style={{flex: 1}}>
    <ScrollView>
      <View style={{ margin: 10, padding: 10, backgroundColor: '#fff' }}>
          
              <Text style={{ fontWeight: 'bold', marginVertical: 10 }}>7Cs Gold and Jewellery LLC-  Privacy Policy</Text>

              <Text style={{ fontWeight: 'bold', marginVertical: 10 }}>Introduction</Text>

              <Text style={{marginVertical: 10, textAlign: 'justify'}}>
                7Cs Gold and Jewellery LLC is committed to being your jewellery partner of choice. We believe that honesty and transparency are at the core of our relationship with all our partners whether vendors or customers. This privacy policy has been developed to provide our valued partners with a clear understanding of how we collect, use, and protect your and business information so you can partner with us with confidence. By visiting our website and/or by using the services provided in this mobile app, you expressly agree to be bound by the terms of this Policy and you consent to our collection, use, and sharing of your information (as defined herein) in accordance with the terms of this policy. We reserve the right to amend, update or replace this Policy anytime without any notice. This Policy is effective from Sept 1, 2015. If you have any questions or comments about this Privacy Policy, please contact us at 7csweb@7cs.com 
              </Text>
              <Text style={{fontWeight: 'bold', marginVertical: 10 }}>Notice of Our Privacy Practices.</Text>
<Text style={{marginVertical: 10, textAlign: 'justify'}}>This Privacy Policy (the “policy”) sets out the basis on which 7Cs Gold and Jewelers LLC and its subsidiaries (collectively "7Cs Gold and Jewellery LLC", "we", "us", "our") collect, use, and disclose information obtained from your use of www.7csgold.com. </Text>
<Text style={{marginVertical: 10, textAlign: 'justify'}}>1)  "Personal/ Professional/ business information" is defined here as any information that can be associated with a specific person/ business and can be used to identify that person.  We do not consider anonymous information to constitute information as it cannot be used to identify a specific person/ business.</Text>
<Text style={{marginVertical: 10, textAlign: 'justify'}}>2)  We collect information from you when you use our mobile app and our website www.7Csgold.com or its related websites and services (including, without limitation, when you supply/buy items or when you telephone or email our customer care team).  By providing us with your information you expressly consent to us processing such information in accordance with the terms of our privacy policy.</Text>
<Text style={{marginVertical: 10, textAlign: 'justify'}}>3)  We reserve the right to amend this policy at any time by posting a revised version on the website.  Your continued use of the site will constitute your agreement to the amended privacy policy.</Text>
<Text style={{marginVertical: 10, textAlign: 'justify'}}>Our privacy policy covers the following topics: </Text>
<Text style={{marginVertical: 10, textAlign: 'justify'}}>1. Our collection of your personal/ business information</Text>
<Text style={{marginVertical: 10, textAlign: 'justify'}}>2. how we use your personal/business information</Text>
<Text style={{marginVertical: 10, textAlign: 'justify'}}>3. our spam policy</Text>
<Text style={{marginVertical: 10, textAlign: 'justify'}}>4. how we protect your personal/business information</Text>
<Text style={{marginVertical: 10, textAlign: 'justify'}}>5. how you can contact us about privacy questions</Text>

<Text style={{fontWeight: 'bold', marginVertical: 10 }}>Our collection of your personal/business information</Text>
<Text style={{marginVertical: 10, textAlign: 'justify'}}>1)  Registration is required to browse all the details of the website.  Once you decide to work with us or we with you, you will be asked to set up an account and provide / business information necessary for processing your supply or order.  This information will include your name, shipping address, email address and/or telephone number to facilitate processing your order.  You may be asked for additional information such as your date of birth or other identifying information that will help us personalize your 7csgold.com experience.</Text>
<Text style={{marginVertical: 10, textAlign: 'justify'}}>2) If required ,we may also need to ask you to verify your identity by providing valid proof of identification (such as a copy of your passport, resident’s visa or permit, national ID card and/or driver’s license or business registration references)</Text>
<Text style={{marginVertical: 10, textAlign: 'justify'}}>3)  To process your order, we will also need financial information from you, such as your credit card and/or bank account details.</Text>
<Text style={{marginVertical: 10, textAlign: 'justify'}}>Following your registration on this mobile app / our site, you should not post any personal information (including any financial information) anywhere on the Site other than on the My Account section of the Site. Restricting the posting of information to the My Account section of the site protects you from the possibility of fraud or identity theft. The posting by you of any personal/ business information anywhere on the site other than on the My Account section of the Site may lead to the suspension of your use of the site.</Text>
<Text style={{marginVertical: 10, textAlign: 'justify'}}>4)  We may use your Internet protocol (IP) address (which is a unique number assigned to your computer server or your Internet service provider (or ISP)) to analyze user trends and improve the administration of the site.</Text>
<Text style={{marginVertical: 10, textAlign: 'justify'}}>5)  We may also collect information about your computer (for example, browser type) and navigation information (for example, the pages you visit on the Site) along with the times that you access the Site.</Text>
<Text style={{marginVertical: 10, textAlign: 'justify'}}>6)  To best serve our customers and vendors, we may collect other information from you or about you such as your contact with our customer care team, reviews or comments posted by you, or results when you respond to a survey.</Text>
<Text style={{marginVertical: 10, textAlign: 'justify'}}>7)  Where we aggregate personal/business information for statistical purposes, your information shall be anonymous unless specified otherwise.</Text>
<Text style={{marginVertical: 10, textAlign: 'justify'}}>How we use your personal/business information</Text>
<Text style={{marginVertical: 10, textAlign: 'justify'}}>1)  We only use your information to provide services and customer support to you; to measure and improve our services to you; to prevent illegal activities and implement our user agreement with you ("Terms of Use"); troubleshoot problems; provide you with promotional emails, and verify information you give us with third parties. For example, we may share some of the information you give us with banks for credit card authorization, business transactions etc, processing and verification services, or with third parties for fraud-screening purposes.</Text>
<Text style={{marginVertical: 10, textAlign: 'justify'}}>2)  Though we make every effort to preserve your privacy, we may need to disclose your personal information to law enforcement agencies, government agencies or other third parties where we are compelled so to do by court order or similar legal procedure; where we are required to disclose your personal information to comply with law; where we are cooperating with an ongoing law enforcement investigation; or where we have a good faith belief that our disclosure of your personal information is necessary to prevent physical harm or financial loss; to report suspected illegal activity; or to investigate a possible violation of our terms of use.</Text>
<Text style={{marginVertical: 10, textAlign: 'justify'}}>3)  In the event of a sale of 7Csgold.com, or any of its affiliates and subsidiaries or any related business assets, your personal information may be disclosed to any potential purchaser for the purposes of the continued provision of the site or otherwise in relation to any such sale.</Text>
<Text style={{marginVertical: 10, textAlign: 'justify'}}>4)  We may share your information with our other group companies to provide joint content and services to you, to help detect illegal acts and/or the violations of our policies. This information can include details of your user ID, feedback ratings and associated comments relating to your use of the Site. Otherwise, we will only disclose your personal information to a third party with your express permission.</Text>
<Text style={{marginVertical: 10, textAlign: 'justify'}}>5)  We do not sell or rent any of your information to third parties in the normal course of doing business and will only share your personal information with third parties in accordance with this privacy policy.</Text>
<Text style={{marginVertical: 10, textAlign: 'justify'}}>6)  By registering on the site, you agree to receive promotional emails about our products and services. If, at any time, you decide that you do not wish to receive any such emails, you can opt out of receiving them by clicking on the link at the bottom of any of the emails or by going to the My Account section of the site.</Text>
<Text style={{marginVertical: 10, textAlign: 'justify'}}>7)  Additionally, we reserve the right to use product reviews and other comments made by you about the site for marketing purposes and by making such comments you expressly consent to our using your comments for marketing purposes.</Text>
<Text style={{fontWeight: 'bold', marginVertical: 10 }}>Our spam policy</Text>
<Text style={{marginVertical: 10, textAlign: 'justify'}}>1)  We do not tolerate spam. To report site-related spam or spoof emails, please forward the email to 7Csweb@7cs.com</Text>
<Text style={{fontWeight: 'bold', marginVertical: 10 }}>Protecting your information</Text>
<Text style={{marginVertical: 10, textAlign: 'justify'}}>By providing us with your information, you consent to the transfer of your encrypted information to, and its storage on, our servers located in the United States of America. We take every precaution to safeguard all your information from unauthorized access, use or disclosure. All information is encrypted. However, the Internet is not a secure medium and no online business/retailer can 100% guarantee the privacy of your information. You must enter your username and password each time you want to access your account or place an order. Choose your password carefully using unique numbers, letters, and special characters. Never share your username and password with anyone. If you are concerned that your username or password has been compromised, please contact our customer care team immediately and ensure you change your password by logging onto the My Account section of the Site.</Text>
<Text style={{fontWeight: 'bold', marginVertical: 10 }}>How you can contact us about privacy questions</Text>
<Text style={{marginVertical: 10, textAlign: 'justify'}}>If you have questions or concerns about our collection and use of your information, please contact our customer care team at 7csweb@7cs.com or +97142342064.</Text>


          
      </View>
      </ScrollView>
    </View>
  
  ); }
}
