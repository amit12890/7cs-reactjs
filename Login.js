import React, { Component } from 'react';
import { Permissions, Notifications } from 'expo';
import { AppRegistry, Image, View, NetInfo, Text, ActivityIndicator, BackHandler, Alert, TextInput, StyleSheet, Button, TouchableHighlight, AsyncStorage, ScrollView, Keyboard, KeyboardAvoidingView } from 'react-native';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import PushNotifications from './push_notifications';
import { NavigationActions } from 'react-navigation';

const USER_ID = 'user_id';
const ACCESS_TOKEN = 'access_token';
const TYPE = 'type';

import Dimensions from 'Dimensions';
const {width, height} = Dimensions.get('window');

export default class Login extends Component {

  constructor() {
    super();

    this.state = {
      loading: true,
      email: "",
      password: "",
      type: "",
      bottom: 0,
      backgroundColor: "#FFFFFF",
      errors: [],
      status: "",
      accessToken: "",
      result: "",
      customerText: "rgb(30,75,113)",
      vendorText: "rgb(30,75,113)"
    }
  }

  componentWillMount() {


    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
    console.log('running');


    console.log(this.props.navigation.state);

    this.verifyToken();

    console.log('stop');
    this.setCustomer()


  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow() {
    // console.log(this.state.bottom);
    this.setState({
      bottom: 130
    });
  }

  _keyboardDidHide() {
    this.setState({
      bottom: 0
    });
  }


  componentDidMount() {


    Notifications.addListener((notification) => {



    });
    NetInfo.isConnected.addEventListener('change', this.handleConnectionChange);

    NetInfo.isConnected.fetch().done(
      (isConnected) => {
        this.setState({
          status: isConnected
        });
      }
    );

  }


  handleConnectionChange = (isConnected) => {
    this.setState({
      status: isConnected
    });
    if (this.state.status == false) {
      Alert.alert('No Internet Connection', 'There is a problem in your internet connectivity.', [{
        text: 'OK',
        onPress: () => BackHandler.exitApp()
      },]);

    }
    console.log(`is connected: ${this.state.status}`);
  }


  async verifyToken() {
    try {
      let accessToken = await AsyncStorage.getItem(ACCESS_TOKEN);
      let uid = await AsyncStorage.getItem(USER_ID);


      if (accessToken) {
        // this.redireu


        let response = await fetch('http://b2bapp.7csgold.com/api/verify_token.php', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({

            id: uid,
            access_token: accessToken

          })
        });
        let res = await response.json();
        if (response.status >= 200 && response.status < 300) {
          if (res.status) {
            // this.getToken();
            console.log("Verifying Token");
            const {navigate} = this.props.navigation;
            try {
              const value = await AsyncStorage.getItem(TYPE);
              if (value !== null) {
                if (value == 'vendor') {
                  console.log("Verifying Vendor");
                  ////dispatch({type:'Reset', actions: [{ type: 'Navigate', routeName: 'Vendor'}], index: 0 });//
                  //navigate('Vendor');
                  const resetAction = NavigationActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({
                      routeName: 'Vendor'
                    })],
                  });

                  this.props.navigation.dispatch(resetAction);
                } else if (value == 'employee') {
                  // dispatch({type:'Reset', actions: [{ type: 'Navigate', routeName: 'Employee'}], index: 0 });
                  //navigate('Employee');
                  const resetAction = NavigationActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({
                      routeName: 'Employee'
                    })],
                  });

                  this.props.navigation.dispatch(resetAction);
                } else {
                  console.log("Verifying Home");
                  //dispatch({type:'Reset', actions: [{ type: 'Navigate', routeName: 'Home'}], index: 0 });
                  //navigate('Home');
                  const resetAction = NavigationActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({
                      routeName: 'Home'
                    })],
                  });

                  this.props.navigation.dispatch(resetAction);
                // navigate('Home');
                }
              }
            } catch (error) {
              this.setState({
                loading: false
              });
            // Error retrieving data
            }


          } else {
            this.setState({
              loading: false
            });
          // this.setState({errors: this.state.errors.concat("Invalid Token")});
          }

        // this.redirect('root');
        } else {
          this.setState({
            loading: false
          });
          let error = res;
        // this.setState({errors: this.state.errors.concat("Invalid Token")});
        }
      } else {
        this.setState({
          loading: false
        });
      }


    } catch (error) {
      this.setState({
        loading: false
      });
    //this.redirect('login');
    }



  }


  async storeToken(responseAccess, responseType, responseId) {

    try {
      await AsyncStorage.multiSet([[ACCESS_TOKEN, responseAccess], [TYPE, responseType], [USER_ID, responseId]]);
    } catch (error) {

      // Error saving data
    }
  }
  async getToken() {
    try {
      const value = await AsyncStorage.getItem(USER_ID);
      if (value !== null) {
        // We have data!!
        console.log(value);

      }
    } catch (error) {

      // Error retrieving data
    }

  }
  async onLoginPressed() {
    this.setState({
      showProgress: true
    });
    this.setState({
      loading: true
    });
    try {
      let response = await fetch('http://b2bapp.7csgold.com/api/login.php', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({

          email: this.state.email,
          password: this.state.password,
          type: this.state.type

        })
      });
      let res = await response.json();
      if (response.status >= 200 && response.status < 300) {

        //Handle success
        if (res.status) {
          let accessToken = res.user.access_code;
          let type = res.user.type;
          let uid = res.user.id;


          //On success we will store the access_token in the AsyncStorage
          this.storeToken(accessToken, type, uid);
          console.log('registering token');
          await PushNotifications();
          this.getToken();
          const {navigate} = this.props.navigation;
          if (type == 'vendor') {
            //  dispatch({type:'Reset', actions: [{ type: 'Navigate', routeName: 'Vendor'}], index: 0 });
            //navigate('Vendor');
            const resetAction = NavigationActions.reset({
              index: 0,
              actions: [NavigationActions.navigate({
                routeName: 'Vendor'
              })],
            });

            this.props.navigation.dispatch(resetAction);
          } else if (type == 'employee') {
            //dispatch({type:'Reset', actions: [{ type: 'Navigate', routeName: 'Employee'}], index: 0 })
            // navigate('Employee');
            const resetAction = NavigationActions.reset({
              index: 0,
              actions: [NavigationActions.navigate({
                routeName: 'Employee'
              })],
            });

            this.props.navigation.dispatch(resetAction);
          } else {
            // dispatch({type:'Reset', actions: [{ type: 'Navigate', routeName: 'Home'}], index: 0 })
            //navigate('Home');
            const resetAction = NavigationActions.reset({
              index: 0,
              actions: [NavigationActions.navigate({
                routeName: 'Home'
              })],
            });

            this.props.navigation.dispatch(resetAction);
          }

        } else {
          this.setState({
            loading: false
          });
          Alert.alert('Login Error', res.error);
        //this.setState({errors: this.state.errors.concat(res.error)});
        }

      //this.redirect('home');
      } else {
        //Handle error
        this.setState({
          loading: false
        });
        let error = res;
        throw error;
        Alert.alert('Login Error', res.error);
      }
    } catch (error) {
      this.setState({
        error: error
      });
      this.setState({
        loading: false
      });
      Alert.alert('Login Error', error);
      this.setState({
        showProgress: false
      });
    }
  }

  async setCustomer() {
    await this.setState({
      type: "customer",
      customerBg: "rgb(30,75,113)",
      vendorBg: "#fff",
      customerText: "#fff",
      vendorText: "rgb(30,75,113)"
    });
    console.log(this.state.type);
  }
  async setVendor() {
    await this.setState({
      type: "vendor",
      customerBg: "#fff",
      vendorBg: "rgb(30,75,113)",
      vendorText: "#fff",
      customerText: "rgb(30,75,113)"
    });
    console.log(this.state.type);
  }
  render() {
    const {navigate} = this.props.navigation;

    if (this.state.loading) {
      return (

        <View style = {styles.activityContainer}>
        <ActivityIndicator
        animating = {this.state.loading}
        color = 'rgb(30,75,113)'
        size = "large"
        style = {styles.activityIndicator}/>
          </View>

        );
    } else {
      return (


        <View style={styles.container}>
               <ScrollView
        showsVerticalScrollIndicator={false}
        automaticallyAdjustContentInsets={false}

        keyboardDismissMode='on-drag'
        >
              <View style={{
          bottom: this.state.bottom,
          height: height - 15
        }}>
              <Image style={{
          flex: 1,
          alignSelf: 'stretch',
          width: null,
        }} source={require('./img/logo.jpg')}/>
              <Text style={styles.heading}>
                {this.state.result.success}
              </Text>


              <View style={{
          bottom: 20
        }}>

                  <TextInput

        underlineColorAndroid={'#eee'}
        onChangeText={ (text) => this.setState({
          email: text
        }) }
        style={styles.input} placeholder="Email">
                  </TextInput>
                  <TextInput secureTextEntry={true}
        underlineColorAndroid={'#eee'}
        onChangeText={ (text) => this.setState({
          password: text
        }) }
        style={styles.input} placeholder="Password">
                  </TextInput>

                   <View style= {{
          marginBottom: 10,
          flexDirection: 'row',
          height: 60
        }}>
                         <TouchableHighlight
        onPress={ () => this.setCustomer() }
        style={{
          backgroundColor: this.state.customerBg,
          height: 60,
          borderWidth: 1,
          borderRadius: 50,
          borderColor: 'rgb(30,75,113)',
          marginRight: 5,
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center'
        }}>
                              <Text style={{
          fontSize: 18,
          color: this.state.customerText
        }}>Customer</Text>
                      </TouchableHighlight>

                         <TouchableHighlight
        onPress={ () => this.setVendor() }
        style={{
          backgroundColor: this.state.vendorBg,
          height: 60,
          flex: 1,
          borderWidth: 1,
          borderRadius: 50,
          borderColor: 'rgb(30,75,113)',
          marginLeft: 5,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center'
        }}>
                              <Text style={{
          fontSize: 18,
          color: this.state.vendorText
        }}>Vendor</Text>
                      </TouchableHighlight>
                  </View>
                  <TouchableHighlight style={styles.button} onPress={ () => this.onLoginPressed() }>
                    <Text style={styles.buttonText}>
                      Sign In
                      </Text>
                  </TouchableHighlight>
                  <TouchableHighlight underlayColor={'#fff'}  onPress={ () => navigate('Reset') }>
                    <Text style={{
          fontSize: 16,
          color: 'rgb(30,75,113)',
          textAlign: 'center',
          marginTop: 5
        }}>
                      Forgot Password?
                      </Text>
                  </TouchableHighlight>

                  <TouchableHighlight underlayColor={'#fff'}  onPress={ () => navigate('Register') }>
                    <Text style={{
          fontSize: 16,
          color: 'rgb(30,75,113)',
          textAlign: 'center',
          marginTop: 5
        }}>
                      Not yet registered? Sign Up
                      </Text>
                  </TouchableHighlight>

              </View>
              </View>

          </ScrollView>
            </View>

        );
    }
  }
}

const Errors = (props) => {
  return (
    <View>
      {props.errors.map((error, i) => <Text key={i} style={styles.error}> {error} </Text>)}
    </View>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'rgb(248,248,248)',

  },
  activityContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 70
  },
  activityIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 80
  },
  input: {
    height: 50,
    width: 300,
    marginTop: 10,
    paddingHorizontal: 10,
    paddingVertical: 10,
    backgroundColor: '#eee',
    borderBottomColor: '#eee',
    borderRadius: 10,
    bottom: 20,
    fontSize: 18,


  },
  button: {
    height: 50,
    backgroundColor: 'rgb(30,75,113)',
    borderRadius: 50,

    alignSelf: 'stretch',
    marginTop: 10,
    marginBottom: 10,
    justifyContent: 'center'
  },

  buttonText: {
    fontSize: 18,
    color: '#FFF',
    alignSelf: 'center'
  },
  heading: {
    fontSize: 30,
  },
  error: {
    color: 'red',
    paddingTop: 10
  },
  loader: {
    marginTop: 20
  }
});
